.. Manuale Operativo documentation master file, created by
   sphinx-quickstart on Sat May 27 11:20:36 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Manuale operativo
=================

.. toctree::
   :maxdepth: 2
   :caption: Contenuti:


   intro
   deploy
   procedure
   backup
