Introduzione
============

Lo scopo di questo documento è descrivere le procedure di
*gestione ordinaria* dell'applicazione **Oric2**.

Il documento è rivolto a *sviluppatori* e *sistemisti* e descrive le procedure
dal punto di vista tecnico.
