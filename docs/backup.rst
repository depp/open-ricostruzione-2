.. _backup_restore:

Backup/Restore
--------------

Non sono necessari particolari operaizoni di backup, dato che il sito
è fondamentalmente una visualizzazione di dati già esistenti in altri
formati.

Tutti i dati possono essere rigenerati attraverso le procedure descritte in
:ref:`procedure`.

