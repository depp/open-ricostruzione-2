.. _procedure:

Procedure operative
-------------------

Sono descritte le procedure operative, per la gestione dell'applicazione.

Per tutte le operazioni, sono utilizzati i *management task* di django,
che devono essere lanciati, come utente oric, all'interno del virutalenv
applicativo.

Prima di ogni comando è quindi necessario effettuare queste operazioni:

.. code-block:: bash

    su - oric
    source environ/bin/activate

Alla fine delle operazioni, si può *disattivare* il virtualenv con:

.. code-block:: bash

    deactivate


Import nuovi dati
=================
I dati, sotto forma di una cartella contenente files CSV, devono essere
raggiungibili e leggibili dall'utente oric.

Supponendo che questi si trovino in `/var/www/virtualhosts/oric2/data/201801/`,
la procedura per l'import può essere lanciata con:

.. code-block:: bash

    python code/project/manage.py csvimport \
    --csvpath=/var/www/virtualhosts/oric2/data/20180201/ \
    --verbosity=2

Il dettaglio delle operazioni viene visualizzato nello standard output.
È possibile aumentare o diminuire la *verbosity* del dettaglio, da 1 a 3,
2 è il valore consigliato.

Dopo l'import dei dati, è necessario ricostruire l'indice Solr e rigenerare
i files CSV per il download nella sezione opendata.


Cambio data di ultimo aggiornamento
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Per cambiare la data di ultimo aggiornamento, occorre **modificare** il file
contenente le variabili di ambiente:

.. code-block:: bash

  su - oric
  vi code/config/.env
  # LAST_UPDATE=YYYY-MM-DD

Fatto questo, occorre, come utente *root*, far ripartire il servere uwsgi:

.. code-block:: bash

  systemctl restart oric2


Ricostruzione indice Solr
=========================
L'indice Solr, che permette la ricerca testuale all'interno dei dati
e la *navigazione a faccette* dei risultati, filtrati per tipologia,
deve necessariamente essere ricostruito dopo l'import dei dati.

La procedura è la seguente:

.. code-block:: bash

    python code/project/manage.py  \
    rebuild_index --noinput --batch-size=500 \
    --verbosity=2


Generazione files CSV sezione opendata
======================================
La sezione `/open-data` del sito, permette il download di files CSV
generati a partire dai dati importati nel sistema.

Questi dati possono essere generati con questa procedura:

.. code-block:: bash

    python code/project/manage.py csvexport --verbosity=2


.. note::
    il file di metadati (`openricostruzione-metadati.xls`), non
    è generato, ma deve essere copiato *manualmente* nella cartella
    `/var/www/virtualhosts/oric/public/media/opendata`.

Il file originale si trova nella cartella deploy.

Gestione modifiche al codice sorgente
=====================================
In seguito a una modifica al codice sorgente che non sia di natura *evolutiva*,
ossia, che non comporti una modifica allo schema del database, le operazioni da
svolgere sono 2:

1. allineamento del codice sorgente
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
A partire dalla directory `/var/www/virtualhosts/oric2/code`,
come utente `oric`:

.. code-block:: bash

    git pull

2. restart del servizio uwsgi
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
In qualsiasi directory, come utente root:

.. code-block:: bash

    systemctl restart oric2

