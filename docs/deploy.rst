.. _deploy:

Deploy in staging/produzione
----------------------------

Questa sezione descrive la procedura di installazione
dell'applicazione web **Openricostruzione 2** in ambiente di staging, su un server
linux, distribuzione **CentOS7**. La procedura è adatta anche all'installazione
sulla distribuzione **Red Hat Enterprise Linux (RHEL)**,
in ambiente di produzione.

Per procedere all'installazione è necessario avere a disposizione i
files elencati:

- ``env``
- ``nginx.conf``
- ``http-vhost.conf``
- ``systemd.uwsgi.service``
    - ``default_solr.in.sh``
- ``solr/*``

Questi files si trovano nella directory deploy, disponibile per il download a questo link:
https://s3-eu-west-1.amazonaws.com/depp-drop/openricostruzione/deploy.zip.
Poiché contengono dettagli riservati, la directory non è presente
nel repository pubblico (su github).

Alla fine della procedura di installazione, in ambiente di
staging, il sito è disponibile su
questo indirizzo: http://openricostruzione2.staging.deppsviluppo.org/

La prima volta, ed ogni volta che ci sono aggiornamenti dei dati,
è necessario effettuare la procedura di import nuovi dati
e le procedure di generazione dell'indice Solr e dei files CSV opendata,
descritte in :ref:`procedure`.

Setup
+++++
Aggiornare le versioni dei pacchetti e dei repository della distribuzione.

.. code-block:: bash

    yum update -y
    yum install -y epel-release

Timezone
++++++++

.. code-block:: bash

    mv /etc/localtime /etc/localtime.bak
    ln -s /usr/share/zoneinfo/Europe/Rome /etc/localtime


Nginx
+++++


.. code-block:: bash

    yum install -y nginx

    # crea utente e setta la home dell'utente
    # alla root applicativa (se ci sono due app, alla parent)
    mkdir -p /var/www/virtualhosts/
    useradd -d /var/www/virtualhosts/ oric
    chown -R oric /var/www/virtualhosts/

    # impersonare l'utente oric
    su - oric

    # crea directory pubbliche (media e static)
    mkdir -p /var/www/virtualhosts/oric2/public/static
    mkdir -p /var/www/virtualhosts/oric2/public/media

    # setta i permessi corretti per le dir pubbliche

    # permessi di lettura per tutti
    chmod -R a+rx /var/www/virtualhosts

    # tornare root
    exit

    # permessi di scrittura per nginx in media (file uploads)
    chgrp nginx /var/www/virtualhosts/oric2/public/media
    chmod g+w /var/www/virtualhosts/oric2/public/media

    # copia configurazione generica nginx
    mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak
    scp deploy/nginx.conf root@oric_server:/etc/nginx/nginx.conf

    # copia virtualhost applicazione (http o https)
    scp deploy/http[s]-vhost.conf \
        root@oric_server:/etc/nginx/conf.d/oric2.conf

    # setta start automatico per nginx
    systemctl enable nginx

    # start o restart di nginx
    systemctl [re]start nginx


Database
++++++++
.. note:: L'installazione del DBMS descritta quì di seguito è necessaria
    solamente per l'ambiente di staging, qualora in ambiente di produzione
    si usi un DBMS installato su un'altra macchina.
    La stringa di connessione al DB è definita nel file `.env`, all'interno
    della directory `config/`.

.. code-block:: bash

    yum -y install postgresql-server
    systemctl enable postgresql
    systemctl start postgresql
    # secure installation, modify pg_hba.conf

.. note:: In ambiente di staging, per comodità,
    è possibile configurare il DB in modo che
    permetta accessi solamente da localhost e non abbia bisogno di
    password neanche per l'utenza root. La configurazione in ambiente di
    produzione non è oggetto di questo documento.

.. code-block:: bash

    # create db
    createdb -Upostgres open_ricostruzione


Django
++++++


Per creare l'ambiente applicativo, è necessario installare i Development
Tools, in modo da permettere al package manager di Python (pip) di
compilare i package necessari, quando richiesto.

.. code-block:: bash

    # required packages
    yum groupinstall -y "Development Tools"
    yum install -y git gettext libxml2-devel libxslt-devel libjpeg-devel

È necessario python 3.5, che sulla CentOS non è installato di default.
Installare il repository seguendo le istruzioni in
https://www.digitalocean.com/community/tutorials/how-to-set-up-and-use-yum-repositories-on-a-centos-6-vps

.. code-block:: bash

    yum install createrepo
    mkdir /opt/ius-repo
    wget https://centos7.iuscommunity.org/ius-release.rpm
    mv ius-release.rpm /opt/ius-repo
    createrepo /opt/ius-repo

    # installare i pacchetti python 3.5
    yum install -y python3.5-devel python3.5-setuptools python3.5-pip

    # impersona oric
    sudo su - oric

    # logs dir
    mkdir -p /var/www/virtualhosts/oric2/logs

    # directory applicativa
    pushd oric2

    # clone del codice dal repository (in dir. code)
    # il branch di default è il master
    git clone https://gitlab.depp.it/depp/open-ricostruzione-2.git code

    # setta permessi per manage.py
    chmod a+x code/project/manage.py

    # crea virtualenv
    python3.5 -m venv environ

    # attiva virtualenv
    source environ/bin/activate

    # installa requirements di python
    pip install -r code/requirements/production.txt

    # copia il file .env
    scp deploy/env root@oric_server:code/config/.env
    # root deve correggere i permessi di .env,
    # riassegnandolo a oric (nessun permesso di lettura)
    [root] chown oric.oric /var/www/virtualhosts/oric2/code/config/.env
    [root] chmod go-r /var/www/virtualhosts/oric2/code/config/.env

    cd ..

    # apply migrations
    python code/project/manage.py migrate \
        --settings=open_ricostruzione.settings.production

    # to test that django is working
    # statics are not served, though
    python code/project/manage.py runserver 0.0.0.0:8000 \
      --settings=open_ricostruzione.settings.production

    # create a superuser (admin/oric2-aris)
    python code/project/manage.py createsuperuser \
      --settings=open_ricostruzione.settings.production

    # copy assets into public dir served by nginx
    python code/project/manage.py collectstatic --noinput \
      --settings=open_ricostruzione.settings.production


uWSGI
+++++

uWSGI è installato all'interno del virtualenv, come utente oric:

.. code-block:: bash

    sudo su - oric
    cd oric2
    source environ/bin/activate

    # installazione
    pip install uwsgi


uWSGI è installato come servizio di sistema e abilitato per il restart,
lo script seguente deve essere lanciato come utente ``root``.

.. code-block:: bash

    # copia dello script di startup
    scp deploy/systemd.uwsgi.service \
      root@oric_server:/lib/systemd/system/oric2.service

    systemctl enable oric2
    systemctl start oric2



Solr
++++
La versione di solr installata è la 7.0 e l'installazione richiede java 1.8.0.
``lsof`` è richiesto da Solr, per stabilizzare la procedura di stop/restart.
Il servizio è installato attraverso una procedura automatizzata, scaricando
il software e lanciando uno script shell, che installa il software come servizio
in modo che sia integrato nella distribuzione CentOS.

.. code-block:: bash

    yum install -y java-1.8.0-openjdk-headless.x86_64
    yum install -y lsof
    wget http://archive.apache.org/dist/lucene/solr/7.0.1/solr-7.0.1.tgz
    tar xzf solr-7.0.1.tgz solr-7.0.1/bin/install_solr_service.sh --strip-components=2
    bash ./install_solr_service.sh solr-7.0.1.tgz

Alla fine della procedura di installazione, è necessario *configurare*
il servizio, modificando il file ``/etc/default/solr.in.sh``,
dove sono specificati i parametri letti in fase di startup, in
particolare, memoria e indirizzo di binding (host).
Si può usare il file nella directory ``deploy`` del materiale
di consegna.

.. code-block:: bash

    scp default_solr.in.sh \
        root@oric2-server:/etc/default/solr.in.sh

È necessario effettuare il binding su localhost, per evitare accessi da remoto
che potrebbero permettere scrittura e cancellazioni dell'indice da remoto.

Infine, il servizio va *abilitato*, per farlo partire in caso
di reboot del server, e fatto partire.

.. code-block:: bash

    systemctl enable solr
    systemctl start solr


.. note:: Per accedere al pannello di amministrazione da remoto è necessario
    utilizzare un tunnel ssh:

    .. code-block:: bash

        ssh root@oric2-server -L 8983:127.0.0.1:8983 -Nv

    A quel punto, si può visitare la url http://localhost:8983.


.. _deploy_configurazione_solr:

Configurazione del core Solr
++++++++++++++++++++++++++++

- Definire una cartella dove tenere configurazione e dati del core solr (ad esempio `/var/solr/oric2`).

- Copiarvi la cartella `solr`, partendo dalla cartella deploy scaricata dalla URL https://s3-eu-west-1.amazonaws.com/depp-drop/openricostruzione/deploy.zip.

    .. code-block:: bash

            cp -r deploy/solr/* /var/solr/oric2/

- Assegnare *ownership* e *permessi* per la cartella solr all'utenza ``solr``.
    Sul server, come root,

    .. code-block:: bash

        cd /var/solr/oric2/
        chown solr.solr -R solr


- Accedere al pannello di gestione Solr sul web

- Creare un core

    .. figure:: /_static/images/solr_core_creation.png
       :width: 40%
       :align: center
       :alt: Finestra di creazione core Solr

       Finestra di creazione core Solr

    specificando queste variabili:

    - ``name`` = `openricostruzione`
    - ``instanceDir`` = ``/var/solr/oric2``
    - ``dataDir`` = ``/var/solr/oric2/data``

- Verificare lo schema nel pannello Solr dedicato al core appena creato.

- Generare l'indice Solr

    Come utente oric, dopo aver attivato il *virtualenv*,

    .. code-block:: bash

        sudo su - oric
        cd oric2
        source environ/bin/activate
        cd code

        python project/manage.py rebuild_index \
          --settings=open_ricostruzione.settings.production \
          --noinput -v3 --batch-size 500


Note
++++

Alla fine delle procedure che richiedono l'attivazione del virtualenv,
può essere invocata la disattivazione con ``deactivate``.

