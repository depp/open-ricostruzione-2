# -*- coding: utf-8 -*-
import numpy as np


class DataClassifier(object):
    def __init__(self, data, classifier_class=None, classifier_args=None):
        if not classifier_class:
            from pysal.esda import mapclassify as mc
            classifier_class = mc.Fisher_Jenks

        self.classifier_class = classifier_class
        self.classifier_args = classifier_args
        self.data = data

        if len(self.data) == 0:
            self.dc = None
        else:
            if len(data) < self.classifier_args['k']:
                self.classifier_args['k'] = len(data)

            self.dc = self.classifier_class(np.array(self.data), **self.classifier_args)

    def get_bins_range(self):
        bins = list(self.dc.bins)
        return [{'from': current, 'to': next} for current, next in zip([-1] + bins, bins)]

    def get_bin(self, value):
        for n, bin_range in enumerate(self.get_bins_range(), 1):
            if bin_range['from'] < value <= bin_range['to']:
                return n
