# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.urls import reverse
from django.utils.functional import cached_property
from django_extensions.db.fields import AutoSlugField
from .managers import InterventoQuerySet, DonazioneQuerySet


class Comune(models.Model):
    id = models.PositiveIntegerField(primary_key=True, db_index=True)
    denominazione = models.CharField(max_length=30)
    codice_istat = models.CharField(max_length=6)
    provincia_denominazione = models.CharField(max_length=15)
    provincia_sigla = models.CharField(max_length=2)
    regione_denominazione = models.CharField(max_length=15)
    regione_codice = models.CharField(max_length=2)
    slug = AutoSlugField(populate_from='denominazione', max_length=30)

    @property
    def denominazione_con_provincia(self):
        return '{} ({})'.format(self.denominazione, self.provincia_sigla)

    @property
    def codice(self):
        return '{}{}'.format(int(self.regione_codice), self.codice_istat)

    @property
    def provincia_codice(self):
        return self.codice_istat[3:]

    def get_absolute_url(self):
        return reverse('comune-detail', kwargs={'slug': self.slug})

    def __str__(self):
        return self.denominazione_con_provincia

    class Meta:
        ordering = ('regione_denominazione', 'provincia_denominazione', 'denominazione')
        verbose_name = 'Comune'
        verbose_name_plural = 'Comuni'


class Soggetto(models.Model):
    id = models.PositiveIntegerField(primary_key=True, db_index=True)
    impresa_codice_fiscale = models.CharField(max_length=16, null=True, blank=True)
    impresa_partita_iva = models.CharField(max_length=11, null=True, blank=True)
    impresa_ragione_sociale = models.CharField(max_length=256, null=True, blank=True)
    anno_denuncia_addetti = models.CharField(max_length=4, null=True, blank=True)
    numero_addetti_familiari = models.PositiveSmallIntegerField(null=True, blank=True)
    numero_addetti_subordinati = models.PositiveSmallIntegerField(null=True, blank=True)
    sede_indirizzo = models.CharField(max_length=256, null=True, blank=True)
    sede_numero_civico = models.CharField(max_length=10, null=True, blank=True)
    sede_cap = models.CharField(max_length=5, null=True, blank=True)
    sede_comune = models.CharField(max_length=50, null=True, blank=True)
    sede_provincia = models.CharField(max_length=2, null=True, blank=True)

    @cached_property
    def interventi(self):
        from django.db.models import Q
        return Intervento.objects.filter(Q(esecutore=self) | Q(progettista=self))

    def get_absolute_url(self):
        return reverse('soggetto-detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.impresa_ragione_sociale

    class Meta:
        verbose_name = 'Impresa / Progettista'
        verbose_name_plural = 'Imprese / Progettisti'


class SoggettoAttuatore(models.Model):
    denominazione = models.CharField(max_length=128, unique=True)
    slug = AutoSlugField(populate_from='denominazione', max_length=128)

    def get_absolute_url(self):
        return reverse('soggettoattuatore-detail', kwargs={'slug': self.slug})

    def __str__(self):
        return self.denominazione

    class Meta:
        verbose_name = 'Soggetto attuatore'
        verbose_name_plural = 'Soggetti attuatori'


class Intervento(models.Model):
    STATO_PRATICA = (
        ('1', 'Decreti Concessione'),
        ('2', 'Richiesta In Istruttoria'),
        ('3', 'Richiesta Futura'),
        # ('4', 'Richiesta Non Approvata'),
        ('4', 'A Programma'),
    )

    STATO_CANTIERE = (
        ('1', 'Cantiere In Corso'),
        ('2', 'Cantiere Completato'),
        ('3', 'Da Avviare (Contributo Assegnato)'),
    )

    cup = models.CharField(max_length=20, null=True, blank=True)
    descrizione_intervento = models.CharField(max_length=1024, null=True, blank=True)

    importo_assegnato = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    importo_pagato = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)

    stato_pratica = models.CharField(max_length=1, choices=STATO_PRATICA)
    stato_cantiere = models.CharField(max_length=1, choices=STATO_CANTIERE, null=True, blank=True)

    indirizzo = models.CharField(max_length=256, null=True, blank=True)
    numero_civico = models.CharField(max_length=20, null=True, blank=True)

    comune = models.ForeignKey(Comune, related_name='interventi', null=True, blank=True)

    latitudine = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    longitudine = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)

    giorni_istruttoria = models.PositiveSmallIntegerField(null=True, blank=True)
    giorni_cantiere = models.SmallIntegerField(null=True, blank=True)

    esecutore = models.ForeignKey(Soggetto, related_name='interventi_esecutore', null=True, blank=True)
    progettista = models.ForeignKey(Soggetto, related_name='interventi_progettista', null=True, blank=True)

    soggetto_attuatore = models.ForeignKey(SoggettoAttuatore, related_name='interventi', null=True, blank=True)

    slug = AutoSlugField(populate_from='intervento_unique_id', max_length=30)

    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    objects = InterventoQuerySet.as_manager()

    @property
    def intervento_unique_id(self):
        return self.content_object.unique_id

    @property
    def indirizzo_completo(self):
        if self.numero_civico:
            return '{}, {}'.format(self.indirizzo, self.numero_civico)
        else:
            return '{}'.format(self.indirizzo)

    def get_absolute_url(self):
        return reverse('intervento-detail', kwargs={'slug': self.slug})

    def __str__(self):
        if self.content_type.model == 'interventoricostruzioneprivata':
            return '{}{}'.format('{} - '.format(self.indirizzo_completo) if self.indirizzo else '', self.content_object.descrizione)
        elif self.content_type.model == 'interventoattivitaproduttiva':
            return self.content_object.beneficiario or 'beneficiario non presente'
        else:
            return self.descrizione_intervento

    class Meta:
        unique_together = ('content_type', 'object_id')
        verbose_name = 'Intervento'
        verbose_name_plural = 'Interventi'


class Pratica(models.Model):
    intervento = models.ForeignKey(Intervento, related_name='pratiche')
    data = models.DateField(null=True, blank=True)
    tipo = models.CharField(max_length=25)
    importo = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)

    class Meta:
        ordering = ('intervento', 'data', 'id')
        verbose_name = 'Pratica'
        verbose_name_plural = 'Pratiche'


class Donazione(models.Model):
    TIPOLOGIA_CEDENTE = (
        ('0', 'Altro'),
        ('1', 'Associazioni'),
        ('2', 'Enti pubblici'),
        ('3', 'Comuni'),
        ('4', 'Cittadini'),
        ('5', 'Aziende'),
        ('6', 'Regioni'),
        ('7', 'Province'),
        ('8', 'SMS solidale'),
    )

    TIPOLOGIA_DONAZIONE = (
        ('1', 'Diretta'),
        ('2', 'Regione'),
    )

    DESTINAZIONE = (
        ('0', 'Ricostruzione pubblica'),
        ('1', 'Acquisto arredi e attrezzature per edifici scolastici'),
        ('2', 'Interventi di incremento resistenza antisismica edifici pubblici'),
        ('3', 'Contributi alle famiglie'),
        ('4', 'Contributi alle associazioni di volontariato'),
        ('5', 'Interventi di manutenzione ordinaria del patrimonio pubblico danneggiato'),
        ('6', 'Attrezzature sportive'),
        ('7', 'Altro'),
        ('8', 'Da destinare'),
    )

    denominazione = models.CharField(max_length=256)
    tipologia_cedente = models.CharField(max_length=2, choices=TIPOLOGIA_CEDENTE)
    tipologia_donazione = models.CharField(max_length=2, choices=TIPOLOGIA_DONAZIONE)
    data = models.DateField()
    importo = models.DecimalField(max_digits=12, decimal_places=2)
    comune = models.ForeignKey(Comune, related_name='donazioni')
    intervento = models.ForeignKey(Intervento, related_name='donazioni', null=True, blank=True)
    destinazione = models.CharField(max_length=1, choices=DESTINAZIONE, null=True, blank=True)

    objects = DonazioneQuerySet.as_manager()

    def __str__(self):
        # return 'ANONIMO' if self.get_tipologia_cedente_display() == 'Cittadini' or not self.denominazione else self.denominazione
        return self.denominazione or 'ANONIMO'

    class Meta:
        ordering = ('-importo', 'data')


class InterventoBase(models.Model):
    IDPREFIX = ''

    interventi = GenericRelation(Intervento, related_query_name='%(class)s')

    @classmethod
    def get_lookup(cls, field):
        return '{}__{}'.format(cls.__name__.lower(), field)

    @property
    def unique_id(self):
        return '{}{}'.format(self.IDPREFIX, self.id)

    class Meta:
        abstract = True
