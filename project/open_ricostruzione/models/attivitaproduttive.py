# -*- coding: utf-8 -*-
from django.db import models
from .general import InterventoBase


class InterventoAttivitaProduttiva(InterventoBase):
    IDPREFIX = 'S'

    TIPOLOGIA_INTERVENTO = (
        ('RD', "Ricostruzione ed eventuale demolizione dell'immobile"),
        ('RD-SM', "Ricostruzione ed eventuale demolizione dell'immobile Solo miglioramento sismico"),
        ('RD-SR', "Ricostruzione ed eventuale demolizione dell'immobile Solo ripristino"),
        ('RM', 'Ripristino e miglioramento sismico'),
        ('RM-RD', "Ripristino e miglioramento sismico Ricostruzione ed eventuale demolizione dell'immobile"),
        ('RM-RD-SM', "Ripristino e miglioramento sismico Ricostruzione ed eventuale demolizione dell'immobile Solo miglioramento sismico"),
        ('RM-RD-SR', "Ripristino e miglioramento sismico Ricostruzione ed eventuale demolizione dell'immobile Solo ripristino"),
        ('RM-RD-SR-SM', "Ripristino e miglioramento sismico Ricostruzione ed eventuale demolizione dell'immobile Solo ripristino Solo miglioramento sismico"),
        ('RM-SM', 'Ripristino e miglioramento sismico Solo miglioramento sismico'),
        ('RM-SR', 'Ripristino e miglioramento sismico Solo ripristino'),
        ('SR', 'Solo ripristino'),
    )

    SETTORE_ECONOMICO = (
        ('A', 'agricoltura'),
        # ('M', 'agrimodena'),
        ('C', 'commercio'),
        ('I', 'industria'),
    )

    AGGREGATION_FILTER = {
        'field': 'settore_economico',
        'options': [(x[0], x[1].capitalize()) for x in SETTORE_ECONOMICO]
    }

    id = models.PositiveIntegerField(primary_key=True, db_index=True)
    numero_protocollo = models.CharField(max_length=13, unique=True, db_index=True)
    tipologia_intervento = models.CharField(max_length=11, choices=TIPOLOGIA_INTERVENTO, null=True, blank=True)
    settore_economico = models.CharField(max_length=1, choices=SETTORE_ECONOMICO)
    interventi_presentati = models.CharField(max_length=80)  # ['Beni strumentali', 'Delocalizzazione definitiva', 'Delocalizzazione temporanea', 'Immobili', 'Prodotti DOP/IGP', 'Scorte']
    data_decreto_concessione = models.DateField(null=True, blank=True)
    beneficiario = models.CharField(max_length=256, null=True, blank=True)
    data_protocollo_domanda = models.DateField()
    contributo_concesso_immobili = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    contributo_concesso_beni_strumentali = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    contributo_concesso_scorte = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    contributo_concesso_delocalizzazione_temporanea = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    contributo_concesso_prodotti = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)

    @property
    def interventi_presentati_display(self):
        return ', '.join(self.interventi_presentati.strip('|').split('|'))

    class Meta:
        verbose_name = 'Intervento di ricostruzione attività produttive'
        verbose_name_plural = 'Interventi di ricostruzione attività produttive'
