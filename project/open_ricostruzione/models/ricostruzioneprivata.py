# -*- coding: utf-8 -*-
from django.db import models
from .general import InterventoBase


class InterventoRicostruzionePrivata(InterventoBase):
    IDPREFIX = 'M'

    TIPOLOGIA_INTERVENTO = (
        ('1', 'Demolizione e Ricostruzione'),
        ('2', 'Riparazione e ripristino'),
        ('3', 'Riparazione e ripristino con miglioramento sismico'),
    )

    TIPOLOGIA_RICOSTRUZIONE = (
        ('D', 'diretta'),
        ('O', 'organizzata'),
    )

    LIVELLO_DANNO = (
        ('1', 'B/C'),
        ('2', 'E leggere (valore E0)'),
        ('3', 'E pesanti (valori E1, E2, E3)'),
    )

    AGGREGATION_FILTER = {
        'field': 'livello_danno',
        'options': list(LIVELLO_DANNO)
    }

    id = models.PositiveIntegerField(primary_key=True, db_index=True)
    numero_mude_padre = models.CharField(max_length=22, unique=True, db_index=True)
    tipologia_intervento = models.CharField(max_length=1, choices=TIPOLOGIA_INTERVENTO, null=True, blank=True)
    tipologia_ricostruzione = models.CharField(max_length=1, choices=TIPOLOGIA_RICOSTRUZIONE)
    livello_operativo = models.CharField(max_length=3, null=True, blank=True)
    livello_danno = models.CharField(max_length=1, choices=LIVELLO_DANNO, null=True, blank=True)
    data_concessione_cup = models.DateField(null=True, blank=True)
    percentuale_avanzamento = models.DecimalField(max_digits=4, decimal_places=3, null=True, blank=True)
    titolare_codice_fiscale = models.CharField(max_length=16, null=True, blank=True)
    titolare_ragione_sociale = models.CharField(max_length=256, null=True, blank=True)
    data_richiesta_contributo = models.DateField(null=True, blank=True)
    costo_intervento = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    tot_ui_abitanti_coinvolti = models.PositiveSmallIntegerField(null=True, blank=True)
    tot_ui_abitative = models.PositiveSmallIntegerField(null=True, blank=True)
    tot_ui_non_principali = models.PositiveSmallIntegerField(null=True, blank=True)
    tot_ui_produttivo = models.PositiveSmallIntegerField(null=True, blank=True)
    tot_ui_commercio = models.PositiveSmallIntegerField(null=True, blank=True)
    tot_ui_uffici = models.PositiveSmallIntegerField(null=True, blank=True)
    tot_ui_deposito = models.PositiveSmallIntegerField(null=True, blank=True)
    sup_utile_ui_abitative = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True)
    sup_accessoria_ui_abitative = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True)
    sup_comuni_ui_abitative = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True)
    sup_utile_ui_non_principali = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True)
    sup_accessoria_ui_non_principali = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True)
    sup_comuni_ui_non_principali = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True)
    sup_utile_ui_produttivo = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True)
    sup_accessoria_ui_produttivo = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True)
    sup_comuni_ui_produttivo = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True)
    sup_utile_ui_commercio = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True)
    sup_accessoria_ui_commercio = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True)
    sup_comuni_ui_commercio = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True)
    sup_utile_ui_uffici = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True)
    sup_accessoria_ui_uffici = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True)
    sup_comuni_ui_uffici = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True)
    sup_utile_ui_deposito = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True)
    sup_accessoria_ui_deposito = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True)
    sup_comuni_ui_deposito = models.DecimalField(max_digits=8, decimal_places=2, null=True, blank=True)

    @property
    def tot_ui_non_abitative(self):
        return self.tot_ui_produttivo + self.tot_ui_commercio + self.tot_ui_uffici + self.tot_ui_deposito

    @property
    def descrizione(self):
        descrizione = 'cod. Mude: {}'.format(self.numero_mude_padre)
        if self.titolare_ragione_sociale:
            descrizione = '{} ({})'.format(self.titolare_ragione_sociale, descrizione)

        return descrizione

    class Meta:
        verbose_name = 'Intervento di ricostruzione delle abitazioni'
        verbose_name_plural = 'Interventi di ricostruzione delle abitazioni'
