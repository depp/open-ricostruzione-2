# -*- coding: utf-8 -*-
from django.db import models
from .general import InterventoBase


class EnteProprietario(models.Model):
    denominazione = models.CharField(max_length=128, unique=True)

    def __str__(self):
        return self.denominazione


class InterventoRicostruzionePubblica(InterventoBase):
    IDPREFIX = 'F'

    TIPOLOGIA_INTERVENTO = (
        ('01', 'ATTREZZATURE CIMITERIALI'),
        ('02', 'ATTREZZATURE CULTURALI'),
        ('03', 'ATTREZZATURE SOCIO SANITARIE'),
        ('04', 'ATTREZZATURE SPORTIVE E RICREATIVE'),
        ('05', 'CHIESE E EDIFICI DI CULTO'),
        ('06', 'EDIFICI ECCLESIASTICI'),
        ('07', 'EDIFICI PUBBLICI'),
        ('08', 'EDILIZIA SCOLASTICA E UNIVERSITÁ'),
        ('09', "INFRASTRUTTURE E MOBILITA'"),
        ('10', 'OPERE DI BONIFICA E IRRIGAZIONE'),
    )

    STATO_FENICE = (
        ('1', 'ASSEGNAZIONE'),
        ('2', 'CANTIERE'),
        ('3', 'SOLO_COFIN'),
        ('4', 'ISTRUTTORIA'),
        ('5', 'CONCLUSA'),
        ('6', 'A PROGRAMMA'),
        ('7', 'NON PRESENTATO'),
    )

    AGGREGATION_FILTER = {
        'field': 'tipologia_intervento',
        'options': [(x[0], x[1].capitalize()) for x in TIPOLOGIA_INTERVENTO]
    }

    id = models.PositiveIntegerField(primary_key=True, db_index=True)
    numero_ordine = models.PositiveSmallIntegerField(unique=True)
    id_programma = models.PositiveSmallIntegerField(unique=True, null=True, blank=True)
    id_piano = models.PositiveSmallIntegerField(unique=True, null=True, blank=True)
    id_intervento = models.PositiveSmallIntegerField(unique=True, null=True, blank=True)
    cig = models.CharField(max_length=10, null=True, blank=True)
    tipologia_intervento = models.CharField(max_length=2, choices=TIPOLOGIA_INTERVENTO)
    stato_fenice = models.CharField(max_length=1, choices=STATO_FENICE)
    ente_proprietario = models.ForeignKey(EnteProprietario, related_name='interventi')
    soggetto_a_tutela = models.BooleanField(default=False)
    decreto_congruita = models.CharField(max_length=256, null=True, blank=True)
    data_decreto_congruita = models.DateField(null=True, blank=True)
    decreto_assegnazione = models.CharField(max_length=256, null=True, blank=True)
    data_decreto_assegnazione = models.DateField(null=True, blank=True)
    data_saldo = models.DateField(null=True, blank=True)
    data_presa_carico = models.DateField(null=True, blank=True)
    data_inizio_lavori = models.DateField(null=True, blank=True)
    data_fine_lavori = models.DateField(null=True, blank=True)
    importo_generale = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    importo_programma = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    importo_piano = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    importo_congruita = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    importo_cofinanziamento = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    importo_cof_assicurazioni = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    importo_cof_donazioni_rer = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    importo_cof_donazioni_comuni = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    importo_cof_fondi_propri = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    importo_cof_altro = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    importo_erogato_cofin = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)

    @property
    def importi_cofinanziamenti(self):
        return [{'nome': lbl, 'importo': getattr(self, cof)} for lbl, cof in (('Assicurazioni', 'importo_cof_assicurazioni'), ('Donazioni RER', 'importo_cof_donazioni_rer'), ('Donazioni Comuni', 'importo_cof_donazioni_comuni'), ('Fondi propri', 'importo_cof_fondi_propri'), ('Altre fonti', 'importo_cof_altro')) if getattr(self, cof, 0)]

    class Meta:
        verbose_name = 'Intervento di ricostruzione pubblica'
        verbose_name_plural = 'Interventi di ricostruzione pubblica'
