# -*- coding: utf-8 -*-
from django.db import models
from .general import InterventoBase


class InterventoAltro(InterventoBase):
    IDPREFIX = 'A'

    CATEGORIA_OGGETTO = (
        ('C', 'Chiese'),
        ('P', 'Edifici pubblici'),
        ('S', 'Impianti sportivi e ricreativi'),
        ('T', 'Moduli abitativi temporanei'),
        ('U', 'Scuole e Università'),
    )

    AGGREGATION_FILTER = {
        'field': 'categoria_oggetto',
        'options': list(CATEGORIA_OGGETTO)
    }

    id = models.PositiveIntegerField(primary_key=True, db_index=True)
    categoria_oggetto = models.CharField(max_length=1, choices=CATEGORIA_OGGETTO)
    atto_concessione = models.CharField(max_length=20, null=True, blank=True)
    data_concessione = models.DateField(null=True, blank=True)
    data_inizio_lavori = models.DateField(null=True, blank=True)
    data_fine_lavori_prevista = models.DateField(null=True, blank=True)
    data_fine_lavori_reale = models.DateField(null=True, blank=True)
    data_intervento = models.DateField(null=True, blank=True)

    class Meta:
        verbose_name = 'Altri interventi'
        verbose_name_plural = 'Altri interventi'
