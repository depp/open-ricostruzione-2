# -*- coding: utf-8 -*-
from django.db import models
from django.db.models import Count, Sum
from django.db.models.functions import Coalesce


class InterventoQuerySet(models.QuerySet):
    def common_aggregations(self):
        data = self.totali()

        data['stati_pratiche'] = self.totale_per_stato_pratica()
        data['totale_pratiche'] = sum(x['value'] for x in data['stati_pratiche'])

        # data['stati_cantieri'] = self.totale_per_stato_cantiere()
        # data['totale_cantieri'] = sum(x['value'] for x in data['stati_cantieri'])

        return data

    def totali(self):
        return self.aggregate(totale_interventi=Count('pk'), totale_importo_assegnato=Coalesce(Sum('importo_assegnato'), 0), totale_importo_pagato=Coalesce(Sum('importo_pagato'), 0))

    def totale_per_stato_pratica(self):
        totali = {x['stato_pratica']: x['totale'] for x in self.values('stato_pratica').annotate(totale=Count('pk'))}
        return [{'name': x[1], 'value': totali.get(x[0], 0)} for x in reversed(self.model.STATO_PRATICA) if x[0] in ('1', '2', '3')]

    def totale_per_stato_cantiere(self):
        totali = {x['stato_cantiere']: x['totale'] for x in self.values('stato_cantiere').annotate(totale=Count('pk'))}
        return [{'name': x[1], 'value': totali.get(x[0], 0)} for x in reversed(self.model.STATO_CANTIERE) if x[0] in ('1', '2')]

    def totali_per_comune(self):
        from .general import Comune
        return Comune.objects.filter(interventi__in=self).annotate(totale_interventi=Count('interventi__pk'), totale_importo_assegnato=Coalesce(Sum('interventi__importo_assegnato'), 0))

    def top_esecutori(self, n=5):
        return [{'id': x['esecutore__pk'], 'name': x['esecutore__impresa_ragione_sociale'], 'value': x['totale']} for x in self.filter(esecutore__isnull=False).values('esecutore__pk', 'esecutore__impresa_ragione_sociale').annotate(totale=Count('pk')).order_by('-totale', 'esecutore__impresa_ragione_sociale')[:n]]

    def top_progettisti(self, n=5):
        return [{'id': x['progettista__pk'], 'name': x['progettista__impresa_ragione_sociale'], 'value': x['totale']} for x in self.filter(progettista__isnull=False).values('progettista__pk', 'progettista__impresa_ragione_sociale').annotate(totale=Count('pk')).order_by('-totale', 'progettista__impresa_ragione_sociale')[:n]]

    def top_soggettiattuatori(self, n=5):
        return [{'id': x['soggetto_attuatore__slug'], 'name': x['soggetto_attuatore__denominazione'], 'value': x['totale']} for x in self.filter(soggetto_attuatore__isnull=False).values('soggetto_attuatore__slug', 'soggetto_attuatore__denominazione').annotate(totale=Count('pk')).order_by('-totale', 'soggetto_attuatore__denominazione')[:n]]

    def top_cantieri(self, n=5):
        return [{'id': o.slug, 'name': str(o), 'value': o.importo_assegnato} for o in self.order_by('-importo_assegnato')[:n] if o.importo_assegnato]


class DonazioneQuerySet(models.QuerySet):
    def common_aggregations(self):
        data = self.totali()

        data['destinazioni'] = self.totale_per_destinazione()
        data['tipologie_cedenti'] = self.totale_per_tipologia_cedente()

        return data

    def totali(self):
        return self.aggregate(totale_donazioni=Count('pk'), totale_importo=Sum('importo'))

    def totale_per_destinazione(self):
        destinazione_map = dict(self.model.DESTINAZIONE)
        return [{'id': x['destinazione'], 'name': destinazione_map[x['destinazione']], 'value': x['totale']} for x in self.values('destinazione').annotate(totale=Sum('importo')).order_by('-totale')]

    def totale_per_tipologia_cedente(self):
        tipologia_cedente_map = dict(self.model.TIPOLOGIA_CEDENTE)
        return [{'id': x['tipologia_cedente'], 'name': tipologia_cedente_map[x['tipologia_cedente']], 'value': x['totale']} for x in self.values('tipologia_cedente').annotate(totale=Count('pk')).order_by('-totale')]
