# -*- coding: utf-8 -*-
from django import forms
from haystack.forms import FacetedSearchForm


class MySearchForm(FacetedSearchForm):
    type = forms.CharField(required=False)
    subtype = forms.CharField(required=False)
    comune = forms.CharField(required=False)
    soggetto_attuatore = forms.CharField(required=False)
    soggetto = forms.CharField(required=False)

    def no_query_found(self):
        return self.searchqueryset.all()

    def search(self):
        sqs = super(MySearchForm, self).search()

        for fld in ('type', 'subtype', 'comune', 'soggetto', 'soggetto_attuatore'):
            if self.cleaned_data[fld]:
                sqs = sqs.filter(**{fld: self.cleaned_data[fld]})

        sqs = sqs.order_by('name_s')

        sqs = sqs.load_all()

        return sqs
