# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
# from django.contrib import admin
from django.views.generic import ListView
from .models import Comune, Soggetto, SoggettoAttuatore
from .views import HomeView, StaticView, RicostruzionePubblicaInterventoListView, RicostruzionePrivataInterventoListView, \
    AttivitaProduttivaInterventoListView, AltroInterventoListView, ComuneDetailView, SoggettoDetailView, InterventoDetailView, \
    MySearchView, DonazioneListView

# admin.autodiscover()


urls = [
    url(r'^$', HomeView.as_view(template_name='pages/home.html'), name='home'),

    url(r'^ricostruzione-pubblica$', RicostruzionePubblicaInterventoListView.as_view(template_name='pages/interventoricostruzionepubblica_list.html'), name='ricostruzione-pubblica'),
    url(r'^ricostruzione-privata$', RicostruzionePrivataInterventoListView.as_view(template_name='pages/interventoricostruzioneprivata_list.html'), name='ricostruzione-privata'),
    url(r'^ricostruzione-attivitaproduttive$', AttivitaProduttivaInterventoListView.as_view(template_name='pages/interventoattivitaproduttive_list.html'), name='ricostruzione-attivitaproduttive'),
    url(r'^ricostruzione-altro$', AltroInterventoListView.as_view(template_name='pages/interventoaltro_list.html'), name='ricostruzione-altro'),

    url(r'^ricostruzione-dove$', ListView.as_view(template_name='pages/comune_list.html', model=Comune), name='comune-list'),

    url(r'^donazioni$', DonazioneListView.as_view(template_name='pages/donazione_list.html'), name='donazione-list'),

    url(r'^comune/(?P<slug>[-\w]+)$', ComuneDetailView.as_view(template_name='pages/comune_detail.html'), name='comune-detail'),
    url(r'^intervento/(?P<slug>[-\w]+)$', InterventoDetailView.as_view(), name='intervento-detail'),
    url(r'^soggetto/(?P<pk>\d+)$', SoggettoDetailView.as_view(template_name='pages/soggetto_detail.html', model=Soggetto), name='soggetto-detail'),
    url(r'^soggettoattuatore/(?P<slug>[-\w]+)$', SoggettoDetailView.as_view(template_name='pages/soggetto_detail.html', model=SoggettoAttuatore), name='soggettoattuatore-detail'),

    url(r'^(?P<page_slug>chi-siamo|faq|contatti|privacy|licenze|open-data)$', StaticView.as_view(), name='static'),

    url(r'^cerca/', MySearchView.as_view(), name='search'),

    # url(r'^admin/', admin.site.urls),
]
urlpatterns = urls

# static and media urls not works with DEBUG = True, see static function.
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG_TOOLBAR:
    import debug_toolbar
    urlpatterns.append(
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )
