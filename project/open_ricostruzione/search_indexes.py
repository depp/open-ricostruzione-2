# -*- coding: utf-8 -*-
from django.utils.text import slugify
from haystack import indexes
from .models import Comune, Soggetto, SoggettoAttuatore, Intervento


class BaseIndex(indexes.SearchIndex):
    text = indexes.CharField(document=True, use_template=True)
    type = indexes.FacetCharField()

    def prepare_type(self, obj):
        return self.get_model().__name__.lower()

    def prepare(self, obj):
        self.prepared_data = super(BaseIndex, self).prepare(obj)

        self.prepared_data['name_s'] = slugify(str(obj))

        return self.prepared_data


class ComuneIndex(BaseIndex, indexes.Indexable):
    def get_model(self):
        return Comune


class SoggettoIndex(BaseIndex, indexes.Indexable):
    def get_model(self):
        return Soggetto


class SoggettoAttuatoreIndex(BaseIndex, indexes.Indexable):
    def get_model(self):
        return SoggettoAttuatore


class InterventoIndex(BaseIndex, indexes.Indexable):
    subtype = indexes.CharField(stored=False)
    comune = indexes.CharField(stored=False)
    soggetto_attuatore = indexes.CharField(stored=False)
    soggetto = indexes.MultiValueField(stored=False)

    def prepare_type(self, obj):
        return obj.content_object.__class__.__name__.lower()

    def prepare_subtype(self, obj):
        return (getattr(obj.content_object, obj.content_object.__class__.AGGREGATION_FILTER['field']) or '').lower()

    def prepare_comune(self, obj):
        return obj.comune.slug if obj.comune else None

    def prepare_soggetto(self, obj):
        return [obj.esecutore_id, obj.progettista_id]

    def prepare_soggetto_attuatore(self, obj):
        return obj.soggetto_attuatore.slug if obj.soggetto_attuatore else None

    def get_model(self):
        return Intervento
