# -*- coding: utf-8 -*-
from collections import defaultdict, OrderedDict
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ImproperlyConfigured
from django.db.models import Sum, Count, F
from django.urls import reverse
from django.views.generic import TemplateView, DetailView, ListView
from haystack.generic_views import FacetedSearchView
from .data_classification import DataClassifier
from .forms import MySearchForm
from .models import Intervento, InterventoRicostruzionePubblica, InterventoRicostruzionePrivata, \
    InterventoAttivitaProduttiva, InterventoAltro, Comune, Donazione, SoggettoAttuatore, Soggetto


SCOPE_CONFIG = {'totale': None, 'pubblica': InterventoRicostruzionePubblica, 'privata': InterventoRicostruzionePrivata, 'attivitaproduttive': InterventoAttivitaProduttiva, 'altro': InterventoAltro}
SCOPE_CONFIG_INVERSE = {v: k for k, v in SCOPE_CONFIG.items()}


class ContentObjectMixin(object):
    @staticmethod
    def populate_content_object(object_list):
        object_list = [object for object in object_list if hasattr(object, 'content_type_id') and hasattr(object, 'object_id')]

        contentobject_pks = defaultdict(list)
        for object in object_list:
            contentobject_pks[object.content_type_id].append(object.object_id)

        contentobject_by_pks = {pk: ct.model_class().objects.in_bulk(contentobject_pks[pk]) for pk, ct in ContentType.objects.in_bulk(contentobject_pks.keys()).items()}

        for object in object_list:
            object.content_object = contentobject_by_pks[object.content_type_id][object.object_id]


class ComuniDataMixin(object):
    @staticmethod
    def get_comuni_data(interventi, scopeidentifier):
        comuni_con_totali = interventi.totali_per_comune()

        dc = DataClassifier([o.totale_importo_assegnato for o in comuni_con_totali], classifier_args={'k': 7})

        return {
            'clusters': dc.get_bins_range(),
            'data': {o.codice: {
                'name': str(o),
                'url': '{}{}'.format(o.get_absolute_url(), '' if scopeidentifier == 'totale' else '#!{}'.format(scopeidentifier)),
                'amount': o.totale_importo_assegnato,
                'number': o.totale_interventi,
                'cluster': dc.get_bin(o.totale_importo_assegnato),
            } for o in comuni_con_totali},
        }


class HomeView(ComuniDataMixin, TemplateView):
    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)

        ctype_for_models = ContentType.objects.get_for_models(*[m for m in SCOPE_CONFIG.values() if m])

        data = OrderedDict()

        for identifier, model in SCOPE_CONFIG.items():
            interventi = Intervento.objects

            if model:
                interventi = interventi.filter(content_type__pk=ctype_for_models[model].id)
                data[identifier] = interventi.common_aggregations()
            else:
                data[identifier] = {
                    'pubblico': interventi.filter(content_type__pk__in=(ctype_for_models[m].id for m in (InterventoRicostruzionePubblica, InterventoAltro))).common_aggregations(),
                    'privato': interventi.filter(content_type__pk__in=(ctype_for_models[m].id for m in (InterventoRicostruzionePrivata, InterventoAttivitaProduttiva))).common_aggregations(),
                }

            data[identifier]['comuni'] = self.get_comuni_data(interventi, identifier)

        context['data'] = data

        context['donazioni'] = Donazione.objects.common_aggregations()

        return context


class InterventoListView(ComuniDataMixin, ListView):
    model = Intervento
    content_object_model = None

    def get_queryset(self):
        if self.content_object_model is None:
            raise ImproperlyConfigured()

        queryset = super(InterventoListView, self).get_queryset()
        queryset = queryset.filter(content_type__pk=ContentType.objects.get_for_model(self.content_object_model).id)

        return queryset

    def get_context_data(self, **kwargs):
        context = super(InterventoListView, self).get_context_data(**kwargs)

        context['search_url'] = '{}?type={}'.format(reverse('search'), self.content_object_model.__name__.lower())

        data = {}
        data['interventi'] = OrderedDict()

        for key, label in [('totale', 'Tutti')] + self.content_object_model.AGGREGATION_FILTER['options']:
            interventi = self.object_list

            if key != 'totale':
                interventi = interventi.filter(**{self.content_object_model.get_lookup(self.content_object_model.AGGREGATION_FILTER['field']): key})

            try:
                data_interventi = interventi.common_aggregations()
                data_interventi['label'] = label
                data_interventi['search_url'] = '{}&subtype={}'.format(context['search_url'], key.lower())

                data_interventi['comuni'] = self.get_comuni_data(interventi, SCOPE_CONFIG_INVERSE[self.content_object_model])
            except:
                pass
            else:
                data['interventi'][key] = data_interventi

        context['data'] = data

        return context


class RicostruzionePubblicaInterventoListView(InterventoListView):
    content_object_model = InterventoRicostruzionePubblica

    def get_context_data(self, **kwargs):
        context = super(RicostruzionePubblicaInterventoListView, self).get_context_data(**kwargs)

        context['data']['totali'] = {
            'programma': self.object_list.aggregate(totale_interventi=Count('pk'), totale_importo=Sum(self.content_object_model.get_lookup('importo_generale'))),
            'finanziati': self.object_list.filter(**{'{}__in'.format(self.content_object_model.get_lookup('stato_fenice')): (1, 2, 3, 4, 5, 7)}).aggregate(totale_interventi=Count('pk'), totale_importo=Sum(F(self.content_object_model.get_lookup('importo_piano')) + F(self.content_object_model.get_lookup('importo_cofinanziamento')))),
            'attuazione': self.object_list.filter(**{'{}__in'.format(self.content_object_model.get_lookup('stato_fenice')): (1, 2, 3, 5)}).aggregate(totale_interventi=Count('pk'), totale_importo=Sum(F(self.content_object_model.get_lookup('importo_congruita')) + F(self.content_object_model.get_lookup('importo_cofinanziamento')))),
        }

        # context['data']['top_esecutori'] = self.object_list.top_esecutori()
        # context['data']['top_progettisti'] = self.object_list.top_progettisti()
        # context['data']['top_soggettiattuatori'] = self.object_list.top_soggettiattuatori()
        # context['data']['top_cantieri'] = self.object_list.top_cantieri()

        context['donazioni'] = Donazione.objects.totali()

        return context


class RicostruzionePrivataInterventoListView(InterventoListView):
    content_object_model = InterventoRicostruzionePrivata

    def get_context_data(self, **kwargs):
        context = super(RicostruzionePrivataInterventoListView, self).get_context_data(**kwargs)

        totali = self.object_list.aggregate(**{x: Sum(self.content_object_model.get_lookup(x)) for x in ['tot_ui_abitanti_coinvolti', 'tot_ui_abitative', 'tot_ui_non_principali', 'tot_ui_produttivo', 'tot_ui_commercio', 'tot_ui_uffici', 'tot_ui_deposito', 'sup_utile_ui_abitative']})
        context['data']['totali'] = {
            'totale_ui_principali': totali['tot_ui_abitative'] - totali['tot_ui_non_principali'],
            'totale_ui_non_principali': totali['tot_ui_non_principali'],
            'totale_ui_abitanti_coinvolti': totali['tot_ui_abitanti_coinvolti'],
            'totale_ui_non_abitative': totali['tot_ui_produttivo'] + totali['tot_ui_commercio'] + totali['tot_ui_uffici'] + totali['tot_ui_deposito'],
            'superficie_ui_abitative': totali['sup_utile_ui_abitative'],
        }

        # context['data']['top_esecutori'] = self.object_list.top_esecutori()
        # context['data']['top_progettisti'] = self.object_list.top_progettisti()

        return context


class AttivitaProduttivaInterventoListView(InterventoListView):
    content_object_model = InterventoAttivitaProduttiva

    def get_context_data(self, **kwargs):
        context = super(AttivitaProduttivaInterventoListView, self).get_context_data(**kwargs)

        context['data']['contributi_concessi'] = [{'name': k.replace('contributo_concesso_', '').replace('_', ' ').capitalize(), 'value': v} for k, v in self.object_list.aggregate(**{x: Sum(self.content_object_model.get_lookup(x)) for x in ['contributo_concesso_immobili', 'contributo_concesso_beni_strumentali', 'contributo_concesso_scorte', 'contributo_concesso_delocalizzazione_temporanea', 'contributo_concesso_prodotti']}).items()]

        # context['data']['top_cantieri'] = self.object_list.top_cantieri()

        return context


class AltroInterventoListView(InterventoListView):
    content_object_model = InterventoAltro


class ComuneDetailView(ContentObjectMixin, DetailView):
    model = Comune

    def get_context_data(self, **kwargs):
        context = super(ComuneDetailView, self).get_context_data(**kwargs)

        context['search_url'] = '{}?comune={}'.format(reverse('search'), self.object.slug)

        ctype_for_models = ContentType.objects.get_for_models(*[m for m in SCOPE_CONFIG.values() if m])

        context['data'] = OrderedDict()

        for identifier, model in SCOPE_CONFIG.items():
            interventi = self.object.interventi

            if model:
                interventi = interventi.filter(content_type__pk=ctype_for_models[model].id)

            data = {}
            data['common'] = interventi.common_aggregations()

            if data['common']['totale_interventi']:
                if identifier == 'totale':
                    interventi = interventi.all()

                    self.populate_content_object(interventi)

                    data['common']['map_info'] = {
                        'codice_comune': self.object.codice,
                        'poi_list': [{'name': str(o), 'url': o.get_absolute_url(), 'type': SCOPE_CONFIG_INVERSE[o.content_object.__class__], 'status': o.stato_cantiere or '3', 'lat': o.latitudine, 'lng': o.longitudine} for o in interventi if o.latitudine and o.longitudine]
                    }
                else:
                    data['search_url'] = '{}&type={}'.format(context['search_url'], model.__name__.lower())

                    data['interventi'] = OrderedDict()
                    for key, label in model.AGGREGATION_FILTER['options']:
                        data['interventi'][key] = interventi.filter(**{model.get_lookup(model.AGGREGATION_FILTER['field']): key}).totali()
                        data['interventi'][key]['label'] = label
                        data['interventi'][key]['search_url'] = '{}&subtype={}'.format(data['search_url'], key.lower())

                    # if identifier in ('pubblica', 'privata'):
                    #     data['top_esecutori'] = interventi.top_esecutori()
                    #     data['top_progettisti'] = interventi.top_progettisti()

                    # if identifier in ('pubblica', 'attivitaproduttive'):
                    #     data['top_cantieri'] = interventi.top_cantieri()

                    # if identifier in ('pubblica',):
                    #     data['top_soggettiattuatori'] = interventi.top_soggettiattuatori()

                    if identifier in ('privata',):
                        totali = interventi.aggregate(**{x: Sum(model.get_lookup(x)) for x in ['tot_ui_abitanti_coinvolti', 'tot_ui_abitative', 'tot_ui_non_principali', 'tot_ui_produttivo', 'tot_ui_commercio', 'tot_ui_uffici', 'tot_ui_deposito', 'sup_utile_ui_abitative']})
                        data['totali'] = {
                            'totale_ui_principali': totali['tot_ui_abitative'] - totali['tot_ui_non_principali'],
                            'totale_ui_non_principali': totali['tot_ui_non_principali'],
                            'totale_ui_abitanti_coinvolti': totali['tot_ui_abitanti_coinvolti'],
                            'totale_ui_non_abitative': totali['tot_ui_produttivo'] + totali['tot_ui_commercio'] + totali['tot_ui_uffici'] + totali['tot_ui_deposito'],
                            'superficie_ui_abitative': totali['sup_utile_ui_abitative'],
                        }

                    if identifier in ('attivitaproduttive',):
                        data['contributi_concessi'] = [{'name': k.replace('contributo_concesso_', '').replace('_', ' ').capitalize(), 'value': v} for k, v in interventi.aggregate(**{x: Sum(model.get_lookup(x)) for x in ['contributo_concesso_immobili', 'contributo_concesso_beni_strumentali', 'contributo_concesso_scorte', 'contributo_concesso_delocalizzazione_temporanea', 'contributo_concesso_prodotti']}).items()]

            context['data'][identifier] = data

        context['donazioni'] = self.object.donazioni.common_aggregations()

        return context


class SoggettoDetailView(DetailView):
    def get_context_data(self, **kwargs):
        context = super(SoggettoDetailView, self).get_context_data(**kwargs)

        if self.model.__name__ == 'Soggetto':
            context['search_url'] = '{}?soggetto={}'.format(reverse('search'), self.object.pk)
        else:
            context['search_url'] = '{}?soggetto_attuatore={}'.format(reverse('search'), self.object.slug)

        interventi = self.object.interventi

        context['data'] = interventi.common_aggregations()
        context['data']['map_info'] = {
            'poi_list': [{'name': str(o), 'url': o.get_absolute_url(), 'type': SCOPE_CONFIG_INVERSE[o.content_object.__class__], 'status': o.stato_cantiere or '3', 'lat': o.latitudine, 'lng': o.longitudine} for o in interventi.all() if o.latitudine and o.longitudine]
        }

        return context


class InterventoDetailView(DetailView):
    model = Intervento

    def get_template_names(self):
        return ['pages/{}_detail.html'.format(self.object.content_object.__class__.__name__.lower())]

    def get_context_data(self, **kwargs):
        context = super(InterventoDetailView, self).get_context_data(**kwargs)

        context['donazioni'] = self.object.donazioni.totali()

        context['map_info'] = {
            'poi_list': [{'type': SCOPE_CONFIG_INVERSE[o.content_object.__class__], 'status': o.stato_cantiere or '3', 'lat': o.latitudine, 'lng': o.longitudine} for o in [self.object] if o.latitudine and o.longitudine]
        }

        return context


class DonazioneListView(ContentObjectMixin, ListView):
    model = Donazione
    paginate_by = 50

    params = None

    def get_queryset(self):
        queryset = super(DonazioneListView, self).get_queryset()
        queryset = queryset.select_related('comune', 'intervento')

        self.params = {}

        tipologia_cedente_id = self.request.GET.get('tipo')
        if tipologia_cedente_id:
            try:
                tipologia_cedente = dict(Donazione.TIPOLOGIA_CEDENTE)[tipologia_cedente_id]
            except:
                pass
            else:
                queryset = queryset.filter(tipologia_cedente=tipologia_cedente_id)
                self.params['tipologia_cedente'] = tipologia_cedente

        destinazione_id = self.request.GET.get('destinazione')
        if destinazione_id:
            try:
                destinazione = dict(Donazione.DESTINAZIONE)[destinazione_id]
            except:
                pass
            else:
                queryset = queryset.filter(destinazione=destinazione_id)
                self.params['destinazione'] = destinazione

        comune_id = self.request.GET.get('comune')
        if comune_id:
            try:
                comune = Comune.objects.get(pk=comune_id)
            except:
                pass
            else:
                queryset = queryset.filter(comune=comune)
                self.params['comune'] = str(comune)

        intervento_id = self.request.GET.get('intervento')
        if intervento_id:
            try:
                intervento = Intervento.objects.get(slug=intervento_id)
            except:
                pass
            else:
                queryset = queryset.filter(intervento=intervento)
                self.params['intervento'] = str(intervento)

        return queryset

    def get_context_data(self, **kwargs):
        context = super(DonazioneListView, self).get_context_data(**kwargs)

        self.populate_content_object([object.intervento for object in context['object_list']])

        context['params'] = self.params

        return context


class MySearchView(ContentObjectMixin, FacetedSearchView):
    form_class = MySearchForm
    facet_fields = ['type']

    def get_context_data(self, **kwargs):
        def get_params(*params_to_remove):
            params = self.request.GET.copy()

            if 'q' not in params:
                params['q'] = ''

            for param in ('page',) + params_to_remove:
                if param in params:
                    del(params[param])

            return params.urlencode()

        context = super(MySearchView, self).get_context_data(**kwargs)

        context['base_url'] = get_params()

        counts = dict(context['facets']['fields']['type'])
        types = OrderedDict((m.__name__.lower(), m) for m in (SoggettoAttuatore, Soggetto, Comune, InterventoRicostruzionePubblica, InterventoRicostruzionePrivata, InterventoAttivitaProduttiva, InterventoAltro))

        context['filters'] = []
        context['types'] = False

        if self.request.GET.get('type') in types:
            model = types[self.request.GET.get('type')]

            name = model._meta.verbose_name
            if 'subtype' in self.request.GET:
                subtype_id2name = {x[0].lower(): x[1] for x in model.AGGREGATION_FILTER['options']}
                if self.request.GET['subtype'] in subtype_id2name:
                    name += ' ({})'.format(subtype_id2name[self.request.GET['subtype']])

            context['filters'].append({'category': 'Tipologia', 'name': name, 'remove_url': get_params('type', 'subtype')})
        else:
            context['types'] = [{'param': p, 'name': m._meta.verbose_name, 'count': counts[p]} for p, m in types.items() if counts.get(p)]

        for model, param, field in ((Comune, 'comune', 'slug'), (Soggetto, 'soggetto', 'pk'), (SoggettoAttuatore, 'soggetto_attuatore', 'slug')):
            if param in self.request.GET:
                try:
                    obj = model.objects.get(**{field: self.request.GET[param]})
                except:
                    pass
                else:
                    context['filters'].append({'category': model._meta.verbose_name, 'name': str(obj), 'remove_url': get_params(param)})

        self.populate_content_object([object.object for object in context['object_list']])

        comuni_by_pk = Comune.objects.in_bulk(o.object.comune_id for o in context['object_list'] if hasattr(o.object, 'comune_id') and not o.object.comune_id is None)
        for object in context['object_list']:
            if hasattr(object.object, 'comune_id'):
                object.object.comune = comuni_by_pk.get(object.object.comune_id)

        return context


class StaticView(TemplateView):
    def get_template_names(self):
        return ['staticpages/{}.html'.format(self.kwargs.get('page_slug'))]
