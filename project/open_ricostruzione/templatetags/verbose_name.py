# -*- coding: utf-8 -*-
from django import template

register = template.Library()


@register.filter
def model_verbose_name(obj):
    return obj._meta.verbose_name
