# -*- coding: utf-8 -*-
import csv
import datetime
import decimal
import locale
import os
from _csv import register_dialect, QUOTE_ALL
from collections import OrderedDict
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.core.management.base import BaseCommand
from ..commands import timeit
from ...models import Intervento, InterventoAltro, InterventoAttivitaProduttiva, InterventoRicostruzionePrivata, InterventoRicostruzionePubblica, Pratica, Soggetto

ELLIPSIS = '... '


def get_repr(value):
    if callable(value):
        return value()
    return value


def get_field(instance, field):
    field_path = field.split('.')
    attr = instance
    for elem in field_path:
        try:
            attr = getattr(attr, elem)
        except AttributeError:
            return None
    return attr


class excel_semicolon(csv.excel):
    """Extends excel Dialect in order to set semicolon as delimiter."""
    delimiter = ';'
    quoting = QUOTE_ALL

register_dialect('excel-semicolon', excel_semicolon)


class Command(BaseCommand):
    help = 'Export data to CSV'

    def add_arguments(self, parser):
        parser.add_argument('--debug-sql', action='store_true', dest='debugsql', default=False)

    @timeit
    def handle(self, *args, **options):
        locale.setlocale(locale.LC_ALL, 'it_IT.UTF-8')

        self.export_interventi_ricostruzioneprivata()
        self.export_interventi_ricostruzionepubblica()
        self.export_interventi_attivitaproduttive()
        self.export_interventi_altriinterventi()
        self.export_pratiche()
        self.export_soggetti()

        if options['debugsql']:
            from django.db import connection
            for n, q in enumerate(connection.queries, 1):
                self.stdout.write('{}: {} ({}ms)'.format(n, q['sql'], q['time']), ending='\n\n')

    def _base_interventi(self, model):
        object_list = Intervento.objects.filter(content_type__pk=ContentType.objects.get_for_model(model).id).select_related('comune', 'esecutore', 'progettista', 'soggetto_attuatore')

        contentobject_by_pk = {o.pk: o for o in model.objects.select_related().all()}
        for object in object_list:
            object.content_object = contentobject_by_pk[object.object_id]

        columns = OrderedDict([
            ('ID', 'intervento_unique_id'),
            ('CUP', 'cup'),
            ('DESCRIZIONE_INTERVENTO', 'descrizione_intervento'),
            ('IMPORTO_ASSEGNATO', 'importo_assegnato'),
            ('IMPORTO_PAGATO', 'importo_pagato'),
            ('STATO_PRATICA', 'get_stato_pratica_display'),
            ('STATO_CANTIERE', 'get_stato_cantiere_display'),
            ('INDIRIZZO', 'indirizzo'),
            ('NUMERO_CIVICO', 'numero_civico'),
            ('COMUNE', 'comune.denominazione'),
            ('LATITUDINE', 'latitudine'),
            ('LONGITUDINE', 'longitudine'),
            ('GIORNI_ISTRUTTORIA', 'giorni_istruttoria'),
            ('GIORNI_CANTIERE', 'giorni_cantiere'),
            ('ESECUTORE', 'esecutore.impresa_ragione_sociale'),
            ('ESECUTORE_ID', 'esecutore_id'),
            ('PROGETTISTA', 'progettista.impresa_ragione_sociale'),
            ('PROGETTISTA_ID', 'progettista_id'),
            ('SOGGETTO_ATTUATORE', 'soggetto_attuatore.denominazione'),
        ])

        return object_list, columns

    def export_interventi_ricostruzioneprivata(self):
        model = InterventoRicostruzionePrivata

        object_list, columns = self._base_interventi(model)

        del(columns['SOGGETTO_ATTUATORE'])

        columns.update(OrderedDict([
            ('NUMERO_MUDE_PADRE', 'content_object.numero_mude_padre'),
            ('TIPOLOGIA_INTERVENTO', 'content_object.get_tipologia_intervento_display'),
            ('TIPOLOGIA_RICOSTRUZIONE', 'content_object.get_tipologia_ricostruzione_display'),
            ('LIVELLO_OPERATIVO', 'content_object.livello_operativo'),
            ('LIVELLO_DANNO', 'content_object.get_livello_danno_display'),
            ('DATA_CONCESSIONE_CUP', 'content_object.data_concessione_cup'),
            ('PERCENTUALE_AVANZAMENTO', 'content_object.percentuale_avanzamento'),
            ('TITOLARE_CODICE_FISCALE', 'content_object.titolare_codice_fiscale'),
            ('TITOLARE_RAGIONE_SOCIALE', 'content_object.titolare_ragione_sociale'),
            ('DATA_RICHIESTA_CONTRIBUTO', 'content_object.data_richiesta_contributo'),
            ('COSTO_INTERVENTO', 'content_object.costo_intervento'),
            ('TOT_UI_ABITANTI_COINVOLTI', 'content_object.tot_ui_abitanti_coinvolti'),
            ('TOT_UI_ABITATIVE', 'content_object.tot_ui_abitative'),
            ('TOT_UI_NON_PRINCIPALI', 'content_object.tot_ui_non_principali'),
            ('TOT_UI_PRODUTTIVO', 'content_object.tot_ui_produttivo'),
            ('TOT_UI_COMMERCIO', 'content_object.tot_ui_commercio'),
            ('TOT_UI_UFFICI', 'content_object.tot_ui_uffici'),
            ('TOT_UI_DEPOSITO', 'content_object.tot_ui_deposito'),
            ('SUP_UTILE_UI_ABITATIVE', 'content_object.sup_utile_ui_abitative'),
            # ('SUP_ACCESSORIA_UI_ABITATIVE', 'content_object.sup_accessoria_ui_abitative'),
            # ('SUP_COMUNI_UI_ABITATIVE', 'content_object.sup_comuni_ui_abitative'),
            ('SUP_UTILE_UI_NON_PRINCIPALI', 'content_object.sup_utile_ui_non_principali'),
            # ('SUP_ACCESSORIA_UI_NON_PRINCIPALI', 'content_object.sup_accessoria_ui_non_principali'),
            # ('SUP_COMUNI_UI_NON_PRINCIPALI', 'content_object.sup_comuni_ui_non_principali'),
            ('SUP_UTILE_UI_PRODUTTIVO', 'content_object.sup_utile_ui_produttivo'),
            # ('SUP_ACCESSORIA_UI_PRODUTTIVO', 'content_object.sup_accessoria_ui_produttivo'),
            # ('SUP_COMUNI_UI_PRODUTTIVO', 'content_object.sup_comuni_ui_produttivo'),
            ('SUP_UTILE_UI_COMMERCIO', 'content_object.sup_utile_ui_commercio'),
            # ('SUP_ACCESSORIA_UI_COMMERCIO', 'content_object.sup_accessoria_ui_commercio'),
            # ('SUP_COMUNI_UI_COMMERCIO', 'content_object.sup_comuni_ui_commercio'),
            ('SUP_UTILE_UI_UFFICI', 'content_object.sup_utile_ui_uffici'),
            # ('SUP_ACCESSORIA_UI_UFFICI', 'content_object.sup_accessoria_ui_uffici'),
            # ('SUP_COMUNI_UI_UFFICI', 'content_object.sup_comuni_ui_uffici'),
            ('SUP_UTILE_UI_DEPOSITO', 'content_object.sup_utile_ui_deposito'),
            # ('SUP_ACCESSORIA_UI_DEPOSITO', 'content_object.sup_accessoria_ui_deposito'),
            # ('SUP_COMUNI_UI_DEPOSITO', 'content_object.sup_comuni_ui_deposito'),
        ]))

        self._export_to_csv('interventi_ricostruzioneprivata.csv', object_list, columns, model._meta.verbose_name_plural)

    def export_interventi_ricostruzionepubblica(self):
        model = InterventoRicostruzionePubblica

        object_list, columns = self._base_interventi(model)

        columns.update(OrderedDict([
            ('NUMERO_ORDINE', 'content_object.numero_ordine'),
            ('ID_PROGRAMMA', 'content_object.id_programma'),
            ('ID_PIANO', 'content_object.id_piano'),
            ('ID_INTERVENTO', 'content_object.id_intervento'),
            ('CIG', 'content_object.cig'),
            ('TIPOLOGIA_INTERVENTO', 'content_object.get_tipologia_intervento_display'),
            # ('STATO_FENICE', 'content_object.get_stato_fenice_display'),
            ('ENTE_PROPRIETARIO', 'content_object.ente_proprietario.denominazione'),
            # ('SOGGETTO_A_TUTELA', 'content_object.soggetto_a_tutela'),
            ('DECRETO_CONGRUITA', 'content_object.decreto_congruita'),
            ('DATA_DECRETO_CONGRUITA', 'content_object.data_decreto_congruita'),
            ('DECRETO_ASSEGNAZIONE', 'content_object.decreto_assegnazione'),
            ('DATA_DECRETO_ASSEGNAZIONE', 'content_object.data_decreto_assegnazione'),
            ('DATA_SALDO', 'content_object.data_saldo'),
            ('DATA_PRESA_CARICO', 'content_object.data_presa_carico'),
            ('DATA_INIZIO_LAVORI', 'content_object.data_inizio_lavori'),
            ('DATA_FINE_LAVORI', 'content_object.data_fine_lavori'),
            ('IMPORTO_GENERALE', 'content_object.importo_generale'),
            ('IMPORTO_PROGRAMMA', 'content_object.importo_programma'),
            ('IMPORTO_PIANO', 'content_object.importo_piano'),
            ('IMPORTO_CONGRUITA', 'content_object.importo_congruita'),
            ('IMPORTO_COFINANZIAMENTO', 'content_object.importo_cofinanziamento'),
            ('IMPORTO_COF_ASSICURAZIONI', 'content_object.importo_cof_assicurazioni'),
            ('IMPORTO_COF_DONAZIONI_RER', 'content_object.importo_cof_donazioni_rer'),
            ('IMPORTO_COF_DONAZIONI_COMUNI', 'content_object.importo_cof_donazioni_comuni'),
            ('IMPORTO_COF_FONDI_PROPRI', 'content_object.importo_cof_fondi_propri'),
            ('IMPORTO_COF_ALTRO', 'content_object.importo_cof_altro'),
            ('IMPORTO_EROGATO_COFIN', 'content_object.importo_erogato_cofin'),
        ]))

        self._export_to_csv('interventi_ricostruzionepubblica.csv', object_list, columns, model._meta.verbose_name_plural)

    def export_interventi_attivitaproduttive(self):
        model = InterventoAttivitaProduttiva

        object_list, columns = self._base_interventi(model)

        del(columns['DESCRIZIONE_INTERVENTO'])
        del(columns['GIORNI_ISTRUTTORIA'])
        del(columns['GIORNI_CANTIERE'])
        del(columns['ESECUTORE'])
        del(columns['ESECUTORE_ID'])
        del(columns['PROGETTISTA'])
        del(columns['PROGETTISTA_ID'])
        del(columns['SOGGETTO_ATTUATORE'])

        columns.update(OrderedDict([
            ('NUMERO_PROTOCOLLO', 'content_object.numero_protocollo'),
            ('TIPOLOGIA_INTERVENTO', 'content_object.get_tipologia_intervento_display'),
            ('SETTORE_ECONOMICO', 'content_object.get_settore_economico_display'),
            ('INTERVENTI_PRESENTATI', 'content_object.interventi_presentati'),
            ('DATA_DECRETO_CONCESSIONE', 'content_object.data_decreto_concessione'),
            ('BENEFICIARIO', 'content_object.beneficiario'),
            ('DATA_PROTOCOLLO_DOMANDA', 'content_object.data_protocollo_domanda'),
            ('CONTRIBUTO_CONCESSO_IMMOBILI', 'content_object.contributo_concesso_immobili'),
            ('CONTRIBUTO_CONCESSO_BENI_STRUMENTALI', 'content_object.contributo_concesso_beni_strumentali'),
            ('CONTRIBUTO_CONCESSO_SCORTE', 'content_object.contributo_concesso_scorte'),
            ('CONTRIBUTO_CONCESSO_DELOCALIZZAZIONE_TEMPORANEA', 'content_object.contributo_concesso_delocalizzazione_temporanea'),
            ('CONTRIBUTO_CONCESSO_PRODOTTI', 'content_object.contributo_concesso_prodotti'),
        ]))

        self._export_to_csv('interventi_attivitaproduttive.csv', object_list, columns, model._meta.verbose_name_plural)

    def export_interventi_altriinterventi(self):
        model = InterventoAltro

        object_list, columns = self._base_interventi(model)

        del(columns['INDIRIZZO'])
        del(columns['NUMERO_CIVICO'])
        del(columns['LATITUDINE'])
        del(columns['LONGITUDINE'])
        del(columns['GIORNI_ISTRUTTORIA'])
        del(columns['GIORNI_CANTIERE'])
        del(columns['ESECUTORE'])
        del(columns['ESECUTORE_ID'])
        del(columns['PROGETTISTA'])
        del(columns['PROGETTISTA_ID'])

        columns.update(OrderedDict([
            ('CATEGORIA_OGGETTO', 'content_object.get_categoria_oggetto_display'),
            ('ATTO_CONCESSIONE', 'content_object.atto_concessione'),
            ('DATA_CONCESSIONE', 'content_object.data_concessione'),
            ('DATA_INIZIO_LAVORI', 'content_object.data_inizio_lavori'),
            ('DATA_FINE_LAVORI_PREVISTA', 'content_object.data_fine_lavori_prevista'),
            ('DATA_FINE_LAVORI_REALE', 'content_object.data_fine_lavori_reale'),
        ]))

        self._export_to_csv('interventi_altriinterventi.csv', object_list, columns, model._meta.verbose_name_plural)

    def export_pratiche(self):
        object_list = Pratica.objects.select_related('intervento').all()

        contentobject_by_pks = {ct.pk: {o.pk: o for o in ct.model_class().objects.all()} for ct in ContentType.objects.get_for_models(InterventoRicostruzionePubblica, InterventoRicostruzionePrivata, InterventoAttivitaProduttiva, InterventoAltro).values()}
        for object in object_list:
            object.intervento.content_object = contentobject_by_pks[object.intervento.content_type_id][object.intervento.object_id]

        columns = OrderedDict([
            ('ID_INTERVENTO', 'intervento.intervento_unique_id'),
            ('DATA', 'data'),
            ('TIPO', 'tipo'),
            ('IMPORTO', 'importo'),
        ])

        self._export_to_csv('pagamenti.csv', object_list, columns, Pratica._meta.verbose_name_plural)

    def export_soggetti(self):
        object_list = Soggetto.objects.all()

        columns = OrderedDict([
            ('ID', 'id'),
            ('CODICE_FISCALE', 'impresa_codice_fiscale'),
            ('PARTITA_IVA', 'impresa_partita_iva'),
            ('RAGIONE_SOCIALE', 'impresa_ragione_sociale'),
            ('INDIRIZZO', 'sede_indirizzo'),
            ('NUMERO_CIVICO', 'sede_numero_civico'),
            ('CAP', 'sede_cap'),
            ('COMUNE', 'sede_comune'),
            ('PROVINCIA', 'sede_provincia'),
        ])

        self._export_to_csv('soggetti.csv', object_list, columns, Soggetto._meta.verbose_name_plural)


    def _export_to_csv(self, filename, object_list, columns, modelname):
        opendata_path = os.path.join(settings.MEDIA_ROOT, 'opendata')
        if not os.path.exists(opendata_path):
            os.mkdir(opendata_path)
        filename = os.path.join(opendata_path, filename)

        try:
            self.stdout.write('Export {} in {}'.format(modelname, filename), ending=ELLIPSIS)

            with open(filename, 'w') as f:
                writer = csv.writer(f, dialect='excel-semicolon')
                writer.writerow(list(columns.keys()))

                for object in object_list:
                    row = []
                    for fld in columns.values():
                        val = get_repr(get_field(object, fld))

                        if val is None:
                            val = ''
                        elif isinstance(val, bool):
                            val = 'X' if val else ''
                        elif isinstance(val, datetime.date):
                            val = val.strftime('%x')
                        elif isinstance(val, decimal.Decimal):
                            val = locale.format('%f', val)
                        else:
                            val = str(val)

                        row.append(val)

                    writer.writerow(row)
        except Exception as e:
            self.stdout.write(self.style.ERROR('ERRORE: {}'.format(e)))
            exit(1)
        else:
            self.stdout.write(self.style.SUCCESS('OK'))
