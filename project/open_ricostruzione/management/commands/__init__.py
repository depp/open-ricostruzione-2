# -*- coding: utf-8 -*-
import datetime


def timeit(method):
    def timed(*args, **kwargs):
        start_time = datetime.datetime.now()

        result = method(*args, **kwargs)

        duration = datetime.datetime.now() - start_time
        seconds = round(duration.total_seconds())

        print(u'Tempo di esecuzione: {:02d}:{:02d}:{:02d}.'.format(int(seconds // 3600), int((seconds % 3600) // 60), int(seconds % 60)))

        return result

    return timed
