# -*- coding: utf-8 -*-
import pandas as pd
from django.contrib.contenttypes.models import ContentType
from django.core.management.base import BaseCommand
from django.db import transaction, connection
from ..commands import timeit
from ...models import Comune, EnteProprietario, Intervento, InterventoAltro, InterventoAttivitaProduttiva, InterventoRicostruzionePrivata, InterventoRicostruzionePubblica, Pratica, Soggetto, SoggettoAttuatore, Donazione

ELLIPSIS = '... '


def convert_float(value):
    if pd.isnull(value) or str(value).strip() == '':
        return None
    else:
        return float(str(value).replace(',', '.'))


def nullify(value):
    if pd.isnull(value) or str(value).strip() == '':
        return None
    else:
        return value


def fix_comune(value):
    if pd.isnull(value) or str(value).strip() == '':
        return None
    else:
        value = value.strip().upper()
        if value == 'REGGIO EMILIA':
            value = "REGGIO NELL'EMILIA"
        elif value == 'CARBONARA PO':
            value = 'CARBONARA DI PO'
        return value


def fix_missing(value):
    if value == '-1':
        return None
    else:
        return value


class Command(BaseCommand):
    help = 'Import data from csv'

    def add_arguments(self, parser):
        parser.add_argument('--csvpath', dest='csvpath', type=str)

    @timeit
    def handle(self, *args, **options):
        csvpath = options['csvpath']

        self.stdout.write(self.style.NOTICE('Truncating existing tables ...'))
        cursor = connection.cursor()
        for table in [
            'comune', 'donazione', 'enteproprietario',
            'intervento', 'interventoaltro', 'interventoattivitaproduttiva',
            'interventoricostruzioneprivata', 'interventoricostruzionepubblica',
            'pratica', 'soggetto', 'soggettoattuatore'
        ]:
            cursor.execute(
                """TRUNCATE TABLE public.open_ricostruzione_%s
                   RESTART IDENTITY
                   CASCADE;""" % table)
        cursor.close()

        self.import_interventi_ricostruzioneprivata(csvpath)
        self.import_interventi_ricostruzionepubblica(csvpath)
        self.import_interventi_attivitaproduttive(csvpath)
        self.import_interventi_altriinterventi(csvpath)
        self.import_donazioni(csvpath)

    def import_interventi_ricostruzioneprivata(self, csvpath):
        model = InterventoRicostruzionePrivata
        id_colname = 'INT_MUDE_IDS'

        df = self.read_csv_dict(csvpath, {'interventi': 'DUR_L_INTERVENTI_MUDE', 'pratiche': 'DUR_F_PRATICHE_MUDE', 'localizzazioni': 'DATA_SET_localizzazioni', 'cantieri': 'DUR_L_CANTIERI', 'comuni': 'DUR_L_COMUNI_ISTAT', 'soggetti': 'DUR_L_SOGGETTI'})

        df['interventi'] = df['interventi'][df['interventi'][id_colname] != '-1']
        df['interventi'] = df['interventi'][df['interventi']['STATO_PRATICA_IDS'].isin(('1', '2'))]
        df['interventi'] = df['interventi'].groupby('NUMERO_MUDE_PADRE', as_index=False).first()  # fix per NUMERO_MUDE_PADRE in (0803502100000966002016, 0803503200001013442017)

        mainpratica_condition = df['pratiche']['TIPO_PRATICA'] == 'RCR'

        df['interventi'] = df['interventi'].merge(
            df['pratiche'][mainpratica_condition],
            on=id_colname,
            how='left',
            suffixes=('', '_1'),
        # ).merge(
        #     df['cantieri'][['CANT_IDS', 'INDIRIZZO', 'NUMERO_CIVICO']],
        #     on='CANT_IDS',
        #     how='left',
        #     suffixes=('', '_2'),
        ).merge(
            df['localizzazioni'][df['localizzazioni']['Dataset'] == 'MUDE'][['RICHIESTA', 'INDIRIZZO', 'CIVICO', 'X_WGS84', 'Y_WGS84']].groupby('RICHIESTA', as_index=False).first(),
            left_on='NUMERO_MUDE_PADRE',
            right_on='RICHIESTA',
            how='left',
            suffixes=('', '_3'),
        )

        self.import_comuni(df['comuni'][df['comuni']['DENOMINAZIONE_COMUNE'].isin(df['interventi']['RICHIESTA_COMUNE'].unique())])
        comune_den2id = {x.denominazione: x.id for x in Comune.objects.all()}
        df['interventi']['COM_IDS'] = df['interventi'].apply(lambda row: comune_den2id.get(row['RICHIESTA_COMUNE']), axis=1)

        self.import_soggetti(df['soggetti'][df['soggetti']['PAR_IMP_IDS'].isin(pd.unique(df['interventi'][['ESECUTORE_IDS', 'PROGETTISTA_IDS']].values.ravel()))])

        tipologiaintervento_desc2id = {x[1]: x[0] for x in model.TIPOLOGIA_INTERVENTO}

        df_count = len(df['interventi'])

        transaction.set_autocommit(False)

        for n, (_, row) in enumerate(df['interventi'].iterrows(), 1):
            id = row[id_colname]

            try:
                self.stdout.write('{}/{} - Creazione {} {}'.format(n, df_count, model._meta.verbose_name, id), ending=ELLIPSIS)

                values = {}
                values['id'] = id
                values['numero_mude_padre'] = row['NUMERO_MUDE_PADRE']
                values['tipologia_intervento'] = tipologiaintervento_desc2id.get(row['TIPOLOGIA_INTERVENTO'])
                values['tipologia_ricostruzione'] = row['DIRETTO_ORGANIZZATO'][0]
                values['livello_operativo'] = row['LIVELLO_OPERATIVO']
                values['livello_danno'] = {'B/C': '1', 'E0': '2', 'E1': '3', 'E2': '3', 'E3': '3'}.get(row['LIVELLO_OPERATIVO'])
                values['data_concessione_cup'] = nullify(row['DATA_CONCESSIONE_CUP'])
                values['percentuale_avanzamento'] = convert_float(row['PERCENTUALE_AVANZAMENTO'])
                values['titolare_codice_fiscale'] = row['CODICE_FISCALE_TITOLARE']
                values['titolare_ragione_sociale'] = row['RAGIONE_SOCIALE_TITOLARE']
                values['data_richiesta_contributo'] = nullify(row['DATA_PRATICA'])
                values['costo_intervento'] = convert_float(row['COSTO_INTERVENTO'])
                values['tot_ui_abitanti_coinvolti'] = convert_float(row['TOT_UI_ABITANTI_COINVOLTI'])
                for i in ['tot', 'sup_utile', 'sup_accessoria', 'sup_comuni']:
                    for j in ['abitative', 'non_principali', 'produttivo', 'commercio', 'uffici', 'deposito']:
                        k = '{}_ui_{}'.format(i, j)
                        values[k] = convert_float(row[k.upper()])

                obj = model.objects.create(**values)

                self.create_intervento(obj, row)
            except Exception as e:
                self.stdout.write(self.style.ERROR('ERRORE: {}'.format(e)))
                exit(1)
            else:
                self.stdout.write(self.style.SUCCESS('OK'))

            if (n % 5000 == 0) or (n == df_count):
                self.stdout.write('{} -----------------> Committing'.format(n))
                transaction.commit()

        transaction.set_autocommit(True)

        self.import_pratiche(df['pratiche'][df['pratiche'][id_colname].isin(df['interventi'][id_colname]) & ~mainpratica_condition][[id_colname, 'DATA_PRATICA', 'TIPO_PRATICA', 'IMPORTO_SAL']], model)

    def import_interventi_ricostruzionepubblica(self, csvpath):
        model = InterventoRicostruzionePubblica
        id_colname = 'INT_FENICE_IDS'

        df = self.read_csv_dict(csvpath, {'interventi': 'DUR_L_INTERVENTI_FENICE', 'pratiche': 'DUR_F_PRATICHE_FENICE', 'localizzazioni': 'DATA_SET_localizzazioni', 'cantieri': 'DUR_L_CANTIERI', 'comuni': 'DUR_L_COMUNI_ISTAT', 'soggetti': 'DUR_L_SOGGETTI'})

        df['interventi'] = df['interventi'][df['interventi'][id_colname] != '-1']
        df['interventi'] = df['interventi'][df['interventi']['STATO_PRATICA_IDS'].isin(('1', '2', '3', '4'))]

        mainpratica_condition = df['pratiche']['TIPO_DECRETO'] == 'assegnazione'

        df['interventi'] = df['interventi'].merge(
            df['pratiche'][mainpratica_condition],
            on=id_colname,
            how='left',
            suffixes=('', '_1'),
        # ).merge(
        #     df['cantieri'][['CANT_IDS', 'INDIRIZZO', 'NUMERO_CIVICO']],
        #     on='CANT_IDS',
        #     how='left',
        #     suffixes=('', '_2'),
        ).merge(
            df['localizzazioni'][df['localizzazioni']['Dataset'] == 'FENICE'][['RICHIESTA', 'INDIRIZZO', 'CIVICO', 'X_WGS84', 'Y_WGS84']].groupby('RICHIESTA', as_index=False).first(),
            left_on='NUMERO_ORDINE',
            right_on='RICHIESTA',
            how='left',
            suffixes=('', '_3'),
        )

        df['interventi'].rename(columns={'DECRETO': 'DECRETO_ASSEGNAZIONE', 'DATA_ASSEGNAZIONE': 'DATA_DECRETO_ASSEGNAZIONE', 'INIZIO_LAVORI': 'DATA_INIZIO_LAVORI', 'FINE_LAVORI': 'DATA_FINE_LAVORI'}, inplace=True)

        self.import_comuni(df['comuni'][df['comuni']['DENOMINAZIONE_COMUNE'].isin(df['interventi']['COMUNE'].unique())])
        comune_den2id = {x.denominazione: x.id for x in Comune.objects.all()}
        df['interventi']['COM_IDS'] = df['interventi'].apply(lambda row: comune_den2id.get(row['COMUNE']), axis=1)

        self.import_soggetti(df['soggetti'][df['soggetti']['PAR_IMP_IDS'].isin(pd.unique(df['interventi'][['ESECUTORE_IDS', 'PROGETTISTA_IDS']].values.ravel()))])

        self.import_soggettiattuatori(df['interventi']['SOGGETTO_ATTUATORE'].drop_duplicates().sort_values(inplace=False))
        soggettoattuatore_den2id = {x.denominazione: x.id for x in SoggettoAttuatore.objects.all()}
        df['interventi']['SOGGETTO_ATTUATORE_IDS'] = df['interventi'].apply(lambda row: soggettoattuatore_den2id.get(row['SOGGETTO_ATTUATORE']), axis=1)

        self.import_entiproprietari(df['interventi']['ENTE_PROPRIETARIO'].drop_duplicates().sort_values(inplace=False))
        enteproprietario_den2id = {x.denominazione: x.id for x in EnteProprietario.objects.all()}
        df['interventi']['ENTE_PROPRIETARIO_IDS'] = df['interventi'].apply(lambda row: enteproprietario_den2id.get(row['ENTE_PROPRIETARIO']), axis=1)

        tipologiaintervento_desc2id = {x[1]: x[0] for x in model.TIPOLOGIA_INTERVENTO}

        df_count = len(df['interventi'])

        transaction.set_autocommit(False)

        for n, (_, row) in enumerate(df['interventi'].iterrows(), 1):
            id = row[id_colname]

            try:
                self.stdout.write('{}/{} - Creazione {} {}'.format(n, df_count, model._meta.verbose_name, id), ending=ELLIPSIS)

                values = {}
                values['id'] = id
                values['numero_ordine'] = row['NUMERO_ORDINE']
                values['tipologia_intervento'] = tipologiaintervento_desc2id.get(row['TIPOLOGIA_INTERVENTO'])
                values['stato_fenice'] = row['STATO_PRATICA_FENICE_IDS']
                values['ente_proprietario_id'] = row['ENTE_PROPRIETARIO_IDS']
                values['soggetto_a_tutela'] = row['TUTELA'] == 'SOGGETTO A TUTELA'
                for k in ['id_programma', 'id_piano', 'id_intervento', 'cig', 'decreto_congruita', 'data_decreto_congruita', 'decreto_assegnazione', 'data_decreto_assegnazione', 'data_saldo', 'data_presa_carico', 'data_inizio_lavori', 'data_fine_lavori']:
                    values[k] = nullify(row[k.upper()])
                for i in ['cofinanziamento', 'congruita', 'cof_assicurazioni', 'cof_fondi_propri', 'cof_altro', 'generale', 'programma', 'piano', 'cof_donazioni_comuni', 'cof_donazioni_rer', 'erogato_cofin']:
                    k = 'importo_{}'.format(i)
                    values[k] = convert_float(row[k.upper()])

                obj = model.objects.create(**values)

                self.create_intervento(obj, row)
            except Exception as e:
                self.stdout.write(self.style.ERROR('ERRORE: {}'.format(e)))
                exit(1)
            else:
                self.stdout.write(self.style.SUCCESS('OK'))

            if (n % 5000 == 0) or (n == df_count):
                self.stdout.write('{} -----------------> Committing'.format(n))
                transaction.commit()

        transaction.set_autocommit(True)

        self.import_pratiche(df['pratiche'][df['pratiche'][id_colname].isin(df['interventi'][id_colname]) & ~mainpratica_condition][[id_colname, 'DATA_DECRETO', 'TIPO_DECRETO', 'IMPORTO_SAL']], model)

    def import_interventi_attivitaproduttive(self, csvpath):
        model = InterventoAttivitaProduttiva
        id_colname = 'INT_SFINGE_IDS'

        df = self.read_csv_dict(csvpath, {'interventi': 'DUR_L_INTERVENTI_SFINGE', 'pratiche': 'DUR_F_PRATICHE_SFINGE', 'immobili': 'DUR_F_IMMOBILI_SFINGE', 'localizzazioni': 'DATA_SET_localizzazioni', 'cantieri': 'DUR_L_CANTIERI', 'comuni': 'DUR_L_COMUNI_ISTAT'})

        df['interventi'] = df['interventi'][df['interventi'][id_colname] != '-1']
        df['interventi'] = df['interventi'][df['interventi']['STATO_PRATICA_IDS'].isin(('1', '2', '3'))]

        mainpratica_condition = df['pratiche']['TIPO_PRATICA'] == 'protocollo principale'

        df['interventi'] = df['interventi'].merge(
            df['pratiche'][mainpratica_condition],
            on=id_colname,
            how='left',
            suffixes=('', '_1'),
        ).merge(
            df['immobili'].groupby(id_colname, as_index=False).first(),
            on=id_colname,
            how='left',
            suffixes=('', '_2'),
        # ).merge(
        #     df['cantieri'][['CANT_IDS', 'INDIRIZZO', 'NUMERO_CIVICO']],
        #     on='CANT_IDS',
        #     how='left',
        #     suffixes=('', '_3'),
        ).merge(
            df['localizzazioni'][df['localizzazioni']['Dataset'] == 'SFINGE'][['RICHIESTA', 'INDIRIZZO', 'CIVICO', 'X_WGS84', 'Y_WGS84']].groupby('RICHIESTA', as_index=False).first(),
            left_on='NUMERO_PROTOCOLLO_PADRE',
            right_on='RICHIESTA',
            how='left',
            suffixes=('', '_4'),
        )

        self.import_comuni(df['comuni'][df['comuni']['COM_IDS'].isin(df['interventi']['COM_IDS'].unique())])

        tipologiaintervento_desc2id = {x[1]: x[0] for x in model.TIPOLOGIA_INTERVENTO}
        settoreeconomico_desc2id = {x[1]: x[0] for x in model.SETTORE_ECONOMICO}

        df_count = len(df['interventi'])

        transaction.set_autocommit(False)

        for n, (_, row) in enumerate(df['interventi'].iterrows(), 1):
            id = row[id_colname]

            try:
                self.stdout.write('{}/{} - Creazione {} {}'.format(n, df_count, model._meta.verbose_name, id), ending=ELLIPSIS)

                values = {}
                values['id'] = id
                values['numero_protocollo'] = row['NUMERO_PROTOCOLLO_PADRE']
                values['tipologia_intervento'] = tipologiaintervento_desc2id.get(row['TIPOLOGIA_INTERVENTO_IMMOBILI'])
                values['settore_economico'] = settoreeconomico_desc2id.get(row['SETTORE_ECONOMICO'])
                values['interventi_presentati'] = row['INTERVENTI_PRESENTATI']
                values['data_decreto_concessione'] = nullify(row['DATA_DECRETO_CONCESSIONE'])
                values['beneficiario'] = row['RAGIONE_SOCIALE_BENEFICIARIO']
                values['data_protocollo_domanda'] = nullify(row['DATA_PROTOCOLLO_DOMANDA'])
                for i in ['immobili', 'beni_strumentali', 'scorte', 'delocalizzazione_temporanea', 'prodotti']:
                    k = 'contributo_concesso_{}'.format(i)
                    values[k] = convert_float(row[k.upper()])

                obj = model.objects.create(**values)

                self.create_intervento(obj, row)
            except Exception as e:
                self.stdout.write(self.style.ERROR('ERRORE: {}'.format(e)))
                exit(1)
            else:
                self.stdout.write(self.style.SUCCESS('OK'))

            if (n % 5000 == 0) or (n == df_count):
                self.stdout.write('{} -----------------> Committing'.format(n))
                transaction.commit()

        transaction.set_autocommit(True)

        self.import_pratiche(df['pratiche'][df['pratiche'][id_colname].isin(df['interventi'][id_colname]) & ~mainpratica_condition][[id_colname, 'DATA_PROTOCOLLO_DOMANDA', 'TIPO_PRATICA', 'IMPORTO_SAL']], model)

    def import_interventi_altriinterventi(self, csvpath):
        model = InterventoAltro
        id_colname = 'INT_ALTRI_INTERVENTI_IDS'

        df = self.read_csv_dict(csvpath, {'interventi': 'DUR_L_ALTRI_INTERVENTI', 'pratiche': 'DUR_F_PRATICHE_ALTRI_INTERVENTI', 'localizzazioni': 'DATA_SET_localizzazioni', 'comuni': 'DUR_L_COMUNI_ISTAT'})

        df['interventi'] = df['interventi'][df['interventi'][id_colname] != '-1']

        df['pratiche'] = df['pratiche'].groupby((id_colname, 'TIPO_INTERVENTO'), as_index=False).first()  # fix per INT_ALTRI_INTERVENTI_IDS in (496, 509)

        mainpratica_condition = df['pratiche']['TIPO_INTERVENTO'] == 'intervento principale'

        df['interventi'] = df['interventi'].merge(
            df['pratiche'][mainpratica_condition],
            on=id_colname,
            how='left',
            suffixes=('', '_1'),
        ).merge(
            df['localizzazioni'][df['localizzazioni']['Dataset'] == 'ALTRO'][['RICHIESTA', 'INDIRIZZO', 'CIVICO', 'X_WGS84', 'Y_WGS84']].groupby('RICHIESTA', as_index=False).first(),
            left_on='ID_INTERVENTO_PADRE',
            right_on='RICHIESTA',
            how='left',
            suffixes=('', '_2'),
        )

        self.import_comuni(df['comuni'][df['comuni']['DENOMINAZIONE_COMUNE'].isin(df['interventi']['COMUNE'].unique())])
        comune_den2id = {x.denominazione: x.id for x in Comune.objects.all()}
        df['interventi']['COM_IDS'] = df['interventi'].apply(lambda row: comune_den2id.get(row['COMUNE']), axis=1)

        self.import_soggettiattuatori(df['interventi']['SOGGETTO_ATTUATORE'].drop_duplicates().sort_values(inplace=False))
        soggettoattuatore_den2id = {x.denominazione: x.id for x in SoggettoAttuatore.objects.all()}
        df['interventi']['SOGGETTO_ATTUATORE_IDS'] = df['interventi'].apply(lambda row: soggettoattuatore_den2id.get(row['SOGGETTO_ATTUATORE']), axis=1)

        categoriaoggetto_desc2id = {x[1]: x[0] for x in model.CATEGORIA_OGGETTO}

        df_count = len(df['interventi'])

        transaction.set_autocommit(False)

        for n, (_, row) in enumerate(df['interventi'].iterrows(), 1):
            id = row[id_colname]

            try:
                self.stdout.write('{}/{} - Creazione {} {}'.format(n, df_count, model._meta.verbose_name, id), ending=ELLIPSIS)

                values = {}
                values['id'] = id
                values['categoria_oggetto'] = categoriaoggetto_desc2id.get(row['CATEGORIA_OGGETTO'])
                for k in ['atto_concessione', 'data_concessione', 'data_inizio_lavori', 'data_fine_lavori_prevista', 'data_fine_lavori_reale', 'data_intervento']:
                    values[k] = nullify(row[k.upper()])

                obj = model.objects.create(**values)

                self.create_intervento(obj, row)
            except Exception as e:
                self.stdout.write(self.style.ERROR('ERRORE: {}'.format(e)))
                exit(1)
            else:
                self.stdout.write(self.style.SUCCESS('OK'))

            if (n % 5000 == 0) or (n == df_count):
                self.stdout.write('{} -----------------> Committing'.format(n))
                transaction.commit()

        transaction.set_autocommit(True)

        self.import_pratiche(df['pratiche'][df['pratiche'][id_colname].isin(df['interventi'][id_colname]) & ~mainpratica_condition][[id_colname, 'DATA_INTERVENTO', 'TIPO_INTERVENTO', 'IMPORTO_SAL']], model)

    @staticmethod
    def create_intervento(obj, row):
        values = {}
        values['content_object'] = obj
        values['cup'] = row['CUP']
        values['importo_assegnato'] = convert_float(row['IMPORTO_ASSEGNATO'])
        values['importo_pagato'] = convert_float(row['IMPORTO_PAGATO'])
        values['stato_pratica'] = nullify(row['STATO_PRATICA_IDS'])
        values['stato_cantiere'] = nullify(row['STATO_CANTIERE_IDS'])
        values['indirizzo'] = nullify(row.get('INDIRIZZO'))
        values['numero_civico'] = nullify(row.get('CIVICO'))
        values['comune_id'] = nullify(row['COM_IDS'])
        values['descrizione_intervento'] = row.get('DESCRIZIONE_INTERVENTO') or row.get('OGGETTO_DESCRIZ_INTERVENTO')
        values['latitudine'] = convert_float(row.get('Y_WGS84'))
        values['longitudine'] = convert_float(row.get('X_WGS84'))
        # values['latitudine'] = convert_float(row.get('Y_WGS84') or row.get('GEO_LAT') or row.get('GEO_X'))
        # values['longitudine'] = convert_float(row.get('X_WGS84') or row.get('GEO_LON') or row.get('GEO_Y'))
        values['giorni_istruttoria'] = convert_float(row.get('GG_ISTRUTTORIA'))
        values['giorni_cantiere'] = convert_float(row.get('GG_CANTIERE'))
        values['esecutore_id'] = nullify(row.get('ESECUTORE_IDS'))
        values['progettista_id'] = nullify(row.get('PROGETTISTA_IDS'))
        values['soggetto_attuatore_id'] = nullify(row.get('SOGGETTO_ATTUATORE_IDS'))

        Intervento.objects.create(**values)

    def import_pratiche(self, df, model):
        ctype = ContentType.objects.get_for_model(model)

        df = df[~df['IMPORTO_SAL'].isin(('', ',00'))]

        df_count = len(df)

        transaction.set_autocommit(False)

        for n, (_, row) in enumerate(df.iterrows(), 1):
            id = row[0]

            try:
                self.stdout.write('{}/{} - Creazione pratica: {}/{}'.format(n, df_count, ctype.model, id), ending=ELLIPSIS)

                values = {}
                values['intervento'] = Intervento.objects.get(content_type__pk=ctype.id, object_id=id)
                values['data'] = nullify(row[1])
                values['tipo'] = row[2]
                values['importo'] = convert_float(row[3])

                Pratica.objects.create(**values)
            except Exception as e:
                self.stdout.write(self.style.ERROR('ERRORE: {}'.format(e)))
                exit(1)
            else:
                self.stdout.write(self.style.SUCCESS('OK'))

            if (n % 5000 == 0) or (n == df_count):
                self.stdout.write('{} -----------------> Committing'.format(n))
                transaction.commit()

        transaction.set_autocommit(True)

    @transaction.atomic
    def import_comuni(self, df):
        df_count = len(df)

        for n, (_, row) in enumerate(df.iterrows(), 1):
            id = row['COM_IDS']

            try:
                self.stdout.write('{}/{} - Creazione comune: {}'.format(n, df_count, id), ending=ELLIPSIS)

                values = {}
                values['denominazione'] = row['DENOMINAZIONE_COMUNE']
                values['codice_istat'] = row['CODICE_ISTAT']
                values['provincia_denominazione'] = row['DENOMINAZIONE_PROVINCIA']
                values['provincia_sigla'] = row['SIGLA_PROVINCIA']
                values['regione_denominazione'] = row['DENOMINAZIONE_REGIONE']
                values['regione_codice'] = row['CODICE_REGIONE']

                _, created = Comune.objects.get_or_create(id=id, defaults=values)
            except Exception as e:
                self.stdout.write(self.style.ERROR('ERRORE: {}'.format(e)))
                exit(1)
            else:
                if created:
                    self.stdout.write(self.style.SUCCESS('OK'))
                else:
                    self.stdout.write(self.style.WARNING('ESISTENTE'))

    @transaction.atomic
    def import_soggetti(self, df):
        df_count = len(df)

        for n, (_, row) in enumerate(df.iterrows(), 1):
            id = row['PAR_IMP_IDS']

            try:
                self.stdout.write('{}/{} - Creazione soggetto: {}'.format(n, df_count, id), ending=ELLIPSIS)

                values = {}
                values['impresa_codice_fiscale'] = row['CODICE_FISCALE_IMPRESA']
                values['impresa_partita_iva'] = row['PARTITA_IVA_IMPRESA']
                values['impresa_ragione_sociale'] = row['RAGIONE_SOCIALE_IMPRESA']
                values['anno_denuncia_addetti'] = row['ANNO_DENUNCIA_ADDETTI']
                values['numero_addetti_familiari'] = nullify(row['NUMERO_ADDETTI_FAMILIARI'])
                values['numero_addetti_subordinati'] = nullify(row['NUMERO_ADDETTI_SUBORIDINATI'])
                values['sede_indirizzo'] = row['INDIRIZZO_SEDE']
                values['sede_numero_civico'] = row['NUMERO_CIVICO_SEDE']
                values['sede_cap'] = row['CAP_SEDE']
                values['sede_comune'] = row['COMUNE_SEDE']
                values['sede_provincia'] = row['PROVINCIA_SEDE']

                _, created = Soggetto.objects.get_or_create(id=id, defaults=values)
            except Exception as e:
                self.stdout.write(self.style.ERROR('ERRORE: {}'.format(e)))
                exit(1)
            else:
                if created:
                    self.stdout.write(self.style.SUCCESS('OK'))
                else:
                    self.stdout.write(self.style.WARNING('ESISTENTE'))

    def import_entiproprietari(self, df):
        self._import_denominazioni(df, EnteProprietario)

    def import_soggettiattuatori(self, df):
        self._import_denominazioni(df, SoggettoAttuatore)

    @transaction.atomic
    def _import_denominazioni(self, df, model):
        df_count = len(df)

        for n, (_, item) in enumerate(df.iteritems(), 1):
            try:
                self.stdout.write('{}/{} - Creazione {}: {}'.format(n, df_count, model._meta.verbose_name, item), ending=ELLIPSIS)

                _, created = model.objects.get_or_create(denominazione=item)
            except Exception as e:
                self.stdout.write(self.style.ERROR('ERRORE: {}'.format(e)))
                exit(1)
            else:
                if created:
                    self.stdout.write(self.style.SUCCESS('OK'))
                else:
                    self.stdout.write(self.style.WARNING('ESISTENTE'))

    def import_donazioni(self, csvpath):
        model = Donazione

        df = self.read_csv_dict(csvpath, {'donazioni': 'donazioni'})

        comune_cod2id = {x.codice_istat: x.id for x in Comune.objects.all()}
        intervento_nord2id = {x.numero_ordine: x.interventi.all()[0].id for x in InterventoRicostruzionePubblica.objects.all()}  # mancano ('000706', '007502', '009574', '009588', '009008', '009599', '009009', '009667') [ ('1525', '365', '1526', '1918', '1447', '1893', '1907', '1976')]

        df_count = len(df['donazioni'])

        transaction.set_autocommit(False)

        for n, (_, row) in enumerate(df['donazioni'].iterrows(), 1):
            try:
                self.stdout.write('{}/{} - Creazione {} {}'.format(n, df_count, model._meta.verbose_name, row['denominazione']), ending=ELLIPSIS)

                values = {}
                values['denominazione'] = row['denominazione']
                values['tipologia_cedente'] = row['tipologia_cedente']
                values['tipologia_donazione'] = row['tipologia_donazione']
                values['data'] = row['data']
                values['importo'] = convert_float(row['importo'])
                values['comune_id'] = comune_cod2id[row['territorio_codice_istat'].zfill(6)]
                values['intervento_id'] = intervento_nord2id.get(int(row['n_ordine'] or '0'))
                values['destinazione'] = row['codice_altra_destinazione'] or '0'

                obj = model.objects.create(**values)
            except Exception as e:
                self.stdout.write(self.style.ERROR('ERRORE: {}'.format(e)))
                exit(1)
            else:
                self.stdout.write(self.style.SUCCESS('OK'))

            if (n % 5000 == 0) or (n == df_count):
                self.stdout.write('{} -----------------> Committing'.format(n))
                transaction.commit()

        transaction.set_autocommit(True)

    def read_csv_dict(self, csvpath, csvdict):
        df = {}

        for k, v in csvdict.items():
            csvfile = '{}/{}.csv'.format(csvpath.rstrip('/'), v)

            try:
                self.stdout.write('Lettura file {}'.format(csvfile), ending=ELLIPSIS)

                df[k] = pd.read_csv(
                    csvfile,
                    sep=';',
                    header=0,
                    low_memory=True,
                    dtype=object,
                    encoding='latin1' if k == 'localizzazioni' else 'utf-8',
                    keep_default_na=False,
                    converters={
                        'COMUNE': fix_comune,
                        'STATO_CANTIERE_IDS': fix_missing,
                        'ESECUTORE_IDS': fix_missing,
                        'PROGETTISTA_IDS': fix_missing,
                        'ID_INTERVENTO': lambda x: None if x == 'NON_PRESENTE_FENICE' else x,
                        'SETTORE_ECONOMICO': lambda x: 'agricoltura' if x == 'agrimodena' else x,
                        'CODICE_FISCALE_IMPRESA': lambda x: x.strip(),
                        'PARTITA_IVA_IMPRESA': lambda x: x.strip(),
                        'TIPOLOGIA_INTERVENTO_IMMOBILI': lambda x: x.strip(),
                    }
                )
            except IOError:
                self.stdout.write(self.style.ERROR('Errore di lettura'))
                exit(1)
            else:
                self.stdout.write(self.style.SUCCESS('OK'))

        return df
