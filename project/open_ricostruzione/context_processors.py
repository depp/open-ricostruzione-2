# -*- coding: utf-8 -*-
import datetime
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils import translation
from django.utils.translation import ugettext_lazy as _


def main_settings(request):
    main_menu = [
        {
            'name': 'Home',
            'url': reverse('home'),
        },
        {
            'name': _(u'Ricostruzione\npubblica'),
            'url': reverse('ricostruzione-pubblica'),
        },
        {
            'name': _(u'Ricostruzione\ndelle abitazioni'),
            'url': reverse('ricostruzione-privata'),
        },
        {
            'name': _(u'Ricostruzione\nattività produttive'),
            'url': reverse('ricostruzione-attivitaproduttive'),
        },
        {
            'name': _(u'Altri interventi'),
            'url': reverse('ricostruzione-altro'),
        },
        {
            'name': _(u'Dove si interviene'),
            'url': reverse('comune-list'),
        },
    ]

    secondary_menu = [
        {
            'name': 'Chi siamo',
            'url': reverse('static', kwargs={'page_slug': 'chi-siamo'}),
        },
        {
            'name': 'Faq',
            'url': reverse('static', kwargs={'page_slug': 'faq'}),
        },
        {
            'name': 'Contatti',
            'url': reverse('static', kwargs={'page_slug': 'contatti'}),
        },
        {
            'name': 'Informazione sulla privacy',
            'url': reverse('static', kwargs={'page_slug': 'privacy'}),
        },
        {
            'name': 'Licenze di utilizzo dei dati',
            'url': reverse('static', kwargs={'page_slug': 'licenze'}),
        },
        {
            'name': 'Open data',
            'url': reverse('static', kwargs={'page_slug': 'open-data'}),
        },
    ]

    request_path = request.path
    for item in main_menu + secondary_menu:
        item['is_active'] = item['url'] == request_path

    return {
        'MAIN_MENU': main_menu,
        'SECONDARY_MENU': secondary_menu,
        'IS_PRODUCTION': all(h not in request.get_host() for h in ('localhost', 'staging')),
        'LAST_UPDATE': settings.LAST_UPDATE
    }
