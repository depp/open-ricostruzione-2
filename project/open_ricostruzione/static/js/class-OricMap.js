OricMap = (function () {
    var _config = null;

    var _init = function (config_map) {
        _config = config_map;

        $.getJSON(_config.geojson_file, function(geojson_data) {
            _config.geojson_data = geojson_data;

            _renderMaps();

            $('#tab-nav').on('shown.bs.tab', 'a', function () {
                _renderMaps();
            });
            $('select#select-tabs').on('shown.bs.tab', 'option', function () {
                _renderMaps();
            });
        });
    };

    var _renderMaps = function () {
        $('.map').not('.leaflet-container').filter(':visible').each(function() {
            var self = $(this);
            if (typeof map_info !== 'undefined') {
                _renderElementMap(self.attr('id'), _filterMarkersData(map_info['poi_list'], self.data('type')), _config.geojson_data, 'codice_comune' in map_info ? map_info['codice_comune'] : false);
            } else {
                _renderThematicMap(self.attr('id'), self.data('clusters'), _integrateGeoJsonData(_config.geojson_data, self.data('extradata')));
            }
        });
    };

    var _filterMarkersData = function (markers_data, type) {
        return $.grep(markers_data, function(v) {
            return (type == 'totale') || (v.type === type);
        });
    };

    var _integrateGeoJsonData = function (geojson_data, extra_data) {
        var integrated_geojson_data = $.extend(true, {}, geojson_data);

        var features = [];
        $.each(integrated_geojson_data.features, function(index, feature) {
            var id = feature.properties.codice;
            if (id in extra_data) {
                $.extend(feature.properties, extra_data[id]);
                features.push(feature);
            }
        });
        integrated_geojson_data.features = features;

        return integrated_geojson_data;
    };

    var _renderThematicMap = function (id, clusters, geojson_data) {
        var map = _initMap(id);

        // set map center and zoom
        map.setView(L.latLng(_config.map_center), _config.map_zoom.initial);

        // set map legend
        var legend = L.control({position: 'bottomright'});
        legend.onAdd = function (map) {
            var items = '';
            for (var i = 0; i < clusters.length; i++) {
                items += L.Util.template(_config.templates.legend_item, {color: _config.cluster_colors[i + 1], from: intword(clusters[i]['from'] + 1), to: intword(clusters[i]['to'])});
            }

            var div = L.DomUtil.create('div', 'legend');

            div.innerHTML = L.Util.template(_config.templates.legend, {items: items});

            return div;
        };
        legend.addTo(map);

        L.geoJSON(geojson_data, {
            style: function (feature) {
                return L.extend(_config.geojson_style, {fillColor: _config.cluster_colors[feature.properties.cluster]});
            }
        }).bindPopup(function (layer) {
            var props = layer.feature.properties;
            return L.Util.template(_config.templates.popup, {name: props.name, number: props.number.formatMoney(0, ',', '.'), amount: props.amount.formatMoney(0, ',', '.'), url: props.url});
        }).addTo(map);
    };

    var _renderElementMap = function (id, markers_data, geojson_data, codice_comune) {
        var map = _initMap(id);

        var polygon;
        if (codice_comune) {
            $.each(geojson_data.features, function(index, feature) {
                var id = feature.properties.codice;
                if (id == codice_comune) {
                    if (feature.geometry.type == 'MultiPolygon') {
                        polygon = L.polygon($.map(feature.geometry.coordinates, function(d) {return [mapPolygon(d)]}), {interactive: false});
                    } else if (feature.geometry.type == 'Polygon') {
                        polygon = L.polygon(mapPolygon(feature.geometry.coordinates), {interactive: false});
                    }
                }
            });
        }
        if (polygon) {
            polygon.setStyle({color: '#FF0000', weight: 1});
            map.addLayer(polygon).fitBounds(polygon.getBounds());
        } else if (markers_data.length == 1) {
            map.setView(L.latLng(markers_data[0]), _config.map_zoom.max);
        } else {
            map.setView(L.latLng(_config.map_center), _config.map_zoom.initial);
        }

        var legend = L.control({position: 'bottomright'});
        legend.onAdd = function (map) {
            var items1 = '';
            $.each(_config.element_types, function(index, type) {
                items1 += L.Util.template(_config.templates2.legend1_item, type);
            });

            var items2 = '';
            $.each(_config.element_statuses, function(index, status) {
                items2 += L.Util.template(_config.templates2.legend2_item, status);
            });

            var div = L.DomUtil.create('div', 'legend');

            div.innerHTML = L.Util.template(_config.templates2.legend, {items1: items1, items2: items2});

            return div;
        };
        legend.addTo(map);

        var markerIcon = _getIcons(), marker, has_popup;
        $.each(markers_data, function(index, marker_data) {
            has_popup = marker_data.name && marker_data.url;
            marker = L.marker([marker_data.lat, marker_data.lng], {icon: markerIcon[marker_data.type + marker_data.status], alt: _config.element_types[marker_data.type].text, interactive: has_popup, riseOnHover: true}).addTo(map);
            if (has_popup) {
                marker.bindPopup(L.Util.template(_config.templates2.popup, {name: marker_data.name, status: _config.element_statuses['s' + marker_data.status].text, url: marker_data.url}));
            }
        });

        function mapPolygon(poly) {
          return $.map(poly, function(line) {
              return $.map(line, function(d) {
                  return [[d[1],d[0]]];
              })
          })
        }
    };

    var _getIcons = function () {
        var ORicIcon = L.Icon.extend({
            options: {
                iconSize:   [25, 31],
                iconAnchor: [12, 30]
            },
        	initialize: function (options) {
                options.iconRetinaUrl = options.iconUrl.replace('.png', '@2x.png');
        		return L.Icon.prototype.initialize.call(this, options);
        	},
        	_getIconUrl: function (name) {
        		return _config.markers_path + L.Icon.prototype._getIconUrl.call(this, name);
        	}
        });

        var icons = {};

        $.each(_config.markers_icon, function(k, v) {
            icons[k] = new ORicIcon({iconUrl: v});
        });

        return icons;
    };

    var _initMap = function (id) {
        var map = L.map(id);

        // set map bounds
        map.setMaxBounds(L.latLngBounds(L.latLng(_config.map_bounds.sw), L.latLng(_config.map_bounds.ne)));

        // set map minZoom and maxZoom
        map.setMinZoom(_config.map_zoom.min);
        map.setMaxZoom(_config.map_zoom.max);

        map.scrollWheelZoom.disable();

        // create the tile layer with correct attribution
        map.addLayer(L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'}));

        return map;
    };

    return {
        init: _init
    };
})();

Number.prototype.formatMoney = function(cc, dd, tt) {
    var n = this,
        c = isNaN(cc = Math.abs(cc)) ? 2 : cc,
        d = dd == undefined ? '.' : dd,
        t = tt == undefined ? ',' : tt,
        s = n < 0 ? '-' : '',
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + '',
        j = i.length > 3 ? i.length % 3 : 0;

    return s + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '');
};

var intword = function(number) {
    number = parseInt(number);
    if (number < 1000) {
        return number.formatMoney(0, ',', '.');
    } else {
        return Math.round(number / 1000).formatMoney(0, ',', '.') + 'mila';
    }
};
