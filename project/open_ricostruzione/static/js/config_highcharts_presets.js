Highcharts.setOptions({
    lang: {
        numericSymbols: [null, ' mln', ' mld'],
        decimalPoint: ',',
        thousandsSep: '.'
    }
});

var config_highcharts_presets = {
    default: {
        chart: {
            backgroundColor: 'rgba(255, 255, 255, 0.01)',
            style: {
                fontFamily: '"Lato", -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif'
            }
        },
        plotOptions: {
            series: {
                events: {
                    legendItemClick: function(e) {
                        e.preventDefault();
                    }
                }
            }
        },
        legend: {
            margin: 0,
            itemStyle: {
                fontSize: '16px',
                fontWeight: 'normal',
                cursor: 'default'
            },
            itemHoverStyle: null
        },
        xAxis: {
            tickLength: 0,
            minorGridLineWidth: 0
        },
        yAxis: {
            labels: {
                enabled: false
            },
            gridLineWidth: 0,
            minorGridLineWidth: 0,
            title: {
                text: null
            }
        },
        title: {
            text: null
        },
        credits: {
            enabled: false
        },
        tooltip: {
            backgroundColor: '#ffffff',
            shadow: false,
            shape: 'square'
        }
    },
    highcharts_col: {
        chart: {
            type: 'column',
//            height: 580,
            height: 340,
            spacingLeft: 0
        },
        colors: [
            '#bf292f'
        ],
        legend: {
            enabled: false
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            labels: {
                enabled: true
            },
            gridLineWidth: 1,
            minorGridLineWidth: 0
        }
    },
    highcharts_2col: {
        chart: {
            type: 'column',
//            height: 280,
            height: 340,
            spacingLeft: 0
        },
        plotOptions: {
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function (event) {
                            location.href = $(this.name).attr('href');
                        }
                    }
                }
            }
        },
        colors: [
            '#dc8b8e',
            '#bf292f'
        ],
        legend: {
            align: 'left',
            verticalAlign: 'top',
            itemMarginBottom: 30
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            labels: {
                enabled: true
            },
            gridLineWidth: 1,
            minorGridLineWidth: 0
        }
    },
//    highcharts_2col_bar: {
//        chart: {
//            type: 'column',
//            inverted: true,
//            spacingTop: 15,
//            spacingRight: 0,
//            spacingBottom: 0,
//            spacingLeft: 0,
//            height: '60'
//        },
//        plotOptions: {
//            series: {
//                stacking: 'normal',
//                pointWidth: 30
//            }
//        },
//        colors: [
//            '#189c29',
//            '#e2d661'
//        ],
//        legend: {
//            align: 'left',
//            verticalAlign: 'top',
//            reversed: true,
//            adjustChartSize: true,
//            navigation: {
//                enabled:false
//            }
//        },
//        xAxis: {
//            visible: false,
//            type: 'category'
//        },
//        yAxis: {
//            gridLineWidth: 0,
//            visible: false
//        }
//    },
    highcharts_3col_bar: {
        chart: {
            type: 'column',
            inverted: true,
            spacingTop: 15,
            spacingRight: 0,
            spacingBottom: 0,
            spacingLeft: 0,
            height: '60',
            events: {
                load: function() {
                    var updated = false;
                    $.each(this.series, function(index, serie) {
                        if (serie.yData[0] == 0) {
                            serie.update({
                                showInLegend: false
                            }, false);
                            updated = true;
                        }
                    });
                    if (updated) {
                        this.redraw();
                    }
                }
            }
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                pointWidth: 30
            }
        },
        colors: [
            '#d45335',
            '#e2d661',
            '#189c29'
        ],
        legend: {
            align: 'left',
            verticalAlign: 'top',
            reversed: true,
            adjustChartSize: true,
            navigation: {
                enabled:false
            }
        },
        xAxis: {
            visible: false,
            type: 'category'
        },
        yAxis: {
            gridLineWidth: 0,
            visible: false
        }
    },
    highcharts_col_stack: {
        chart: {
            type: 'column',
            inverted: true,
            spacingTop: 0,
            spacingBottom: 0,
            spacingLeft: 0,
            marginBottom: 0,
            height: '200'
        },
        plotOptions: {
            series: {
                cursor: 'pointer',
                colorByPoint: true,
                pointWidth: 20,
                point: {
                    events: {
                        click: function (event) {
                            location.href = $(this.name).attr('href');
                        }
                    }
                }
            }
        },
        colors: [
            '#bf292f'
        ],
        legend: {
            verticalAlign: 'top',
            squareSymbol: false,
            symbolWidth: 0,
            symbolPadding: 0,
            itemStyle: {
                fontWeight: 'normal',
                fontStyle: 'italic',
                fontSize: '14px',
                color: '#bf292f'
            }
        },
        xAxis: {
            lineColor: 'transparent',
            type: 'category',
            labels: {
                style: {
                    fontSize: '16px'
                }
            }
        },
        tooltip: {
            style: {
                fontSize: '10px'
            }
        }
    },
    highcharts_2col_stack: {
        chart: {
            type: 'column',
            inverted: true,
            spacingTop: 20,
            spacingBottom: 0,
            marginBottom: 0,
            height: '90'
        },
        plotOptions: {
            series: {
                colorByPoint: true,
                pointWidth: 27
            }
        },
        colors: [
            '#dc8b8e',
            '#bf292f'
        ],
        legend: {
            enabled: false
        },
        xAxis: {
            lineColor: 'transparent',
            type: 'category',
            labels: {
                style: {
                    fontSize: '16px'
                }
            }
        }
    },
    highcharts_treemap: {
        chart: {
            type: 'treemap',
            spacingRight: 0,
            spacingLeft: 0,
            height: 400
        },
        plotOptions: {
            treemap: {
                dataLabels: {
                    enabled: false
                },
                point: {
                    events: {
                        legendItemClick: function(e) {
                            location.href = $(this.name).attr('href');
                            e.preventDefault();
                        },
                        click: function (event) {
                            location.href = $(this.name).attr('href');
                        }
                    }
                }
            },
            series: {
                cursor: 'pointer',
                colorByPoint: true,
                levels: [
                    {level: 1, borderColor: '#ffffff'}
                ]
            }
        },
        colors: ['#bcd5f5', '#80b1e2', '#5895d9', '#397ac8', '#2461bc', '#164ead', '#0b3fa2', '#021854', '#010e2f'],
        series: [{
            legendType: 'point',
            showInLegend: true
        }],
        legend: {
            align: 'left',
            itemMarginBottom: 10,
            itemStyle: {
                cursor: 'pointer'
            }
        }
    },
    highcharts_pie: {
        chart: {
            type: 'pie',
            height: 280
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: true,
                showInLegend: true,
                dataLabels: {
                    enabled: false
                },
                point: {
                    events: {
                        legendItemClick: function(e) {
                            e.preventDefault();
                        }
                    }
                }
            }
        },
        colors: [
            '#dc8b8e',
            '#bf292f'
        ],
        legend: {
            align: 'left',
            verticalAlign: 'top',
            itemMarginBottom: 30

        },
        xAxis: {
            type: 'category'
        }
    }
};
