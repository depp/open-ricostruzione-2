var config_map = {
    geojson_file: '/static/js/comuni.json',
    markers_path: '/static/img/markers/',
    markers_icon: {
        pubblica1:           'pubblica_1.png',
        pubblica2:           'pubblica_2.png',
        pubblica3:           'pubblica_3.png',
        privata1:            'privata_1.png',
        privata2:            'privata_2.png',
        privata3:            'privata_3.png',
        attivitaproduttive1: 'attivitaproduttive_1.png',
        attivitaproduttive2: 'attivitaproduttive_2.png',
        attivitaproduttive3: 'attivitaproduttive_3.png',
        altro1:              'altro_1.png',
        altro2:              'altro_2.png',
        altro3:              'altro_3.png'
    },
    map_bounds: {
        sw: {
            lat: 43.5,
            lng: 9.0
        },
        ne: {
            lat: 46,
            lng: 13.0
        }
    },
    map_center: {
        lat: 44.75,
        lng: 11.5
    },
    map_zoom: {
        min: 8,
        max: 18,
        initial: 9
    },
    geojson_style: {
        weight: 1,
        opacity: 1,
        color: '#666666',
        fillOpacity: 0.7
    },
    cluster_colors: ['#e3e3e2', '#e6b4b7', '#de6a72', '#c43438', '#af292e', '#a22428', '#94272d', '#711a1e'],
    element_statuses: {
        s3: {color: '#bebebe', text: 'da avviare'},
        s1: {color: '#e2d661', text: 'in corso'},
        s2: {color: '#189c29', text: 'completato'}
    },
    element_types: {
        pubblica: {image: '/static/img/markers/pubblica_3@2x.png', text: 'ricostruzione pubblica'},
        privata: {image: '/static/img/markers/privata_3@2x.png', text: 'ricostruzione delle abitazioni'},
        attivitaproduttive: {image: '/static/img/markers/attivitaproduttive_3@2x.png', text: 'ricostruzione attività produttive'},
        altro: {image: '/static/img/markers/altro_3@2x.png', text: 'altri interventi'}
    },
    templates: {
        legend: '<h4>Risorse assegnate</h4><ul>{items}</ul>',
        legend_item: '<li><i style="background-color: {color}"></i> € {from} - € {to}</li>',
        popup: '<h4>{name}</h4><b>{number} interventi per € {amount} di risorse assegnate</b><br/><a href="{url}" class="btn btn-danger">Scopri di più</a>'
    },
    templates2: {
        legend: '<h4>Ambito della ricostruzione</h4><ul>{items1}</ul><h4>Stato del cantiere</h4><ul>{items2}</ul>',
        legend1_item: '<li><span style="background-image: url({image})"></span> {text}</li>',
        legend2_item: '<li><i style="background-color: {color}"></i> {text}</li>',
        popup: '<h4>{name}</h4><div>Stato del cantiere: {status}</div><br/><a href="{url}" class="btn btn-danger">Scopri di più</a>'
    }
};
