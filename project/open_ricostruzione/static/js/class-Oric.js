Oric = (function () {
    var _config = null;

    var _init = function (OricClassConfig) {
        _config = OricClassConfig;
        _activateBootstrapTabs();
        _renderChartsFromTables();
        _bindActions();
    };

    var _bindActions = function () {
        var sidebar = $('#sidebar-nav');

        $('#menu-toggle').on('click', function(e) {
            e.preventDefault();
            sidebar.css({width: '250px'});
        });

        sidebar.find('.closebtn').on('click', function(e) {
            e.preventDefault();
            sidebar.css({width: '0'});
        });
    };

    var _renderChartsFromTables = function () {
        var cls = 'hc-done';

        $.each(_config.highcharts_presets,
            // for each existing preset
            function (selector, value) {
                // find all the data tables
                $('table.' + selector).not('.' + cls).filter(':visible').each(function() {
                    // get the ID
                    var tableID = $(this).prop('id');

                    // create a wrapper for the chart and attach it to the DOM
                    var highchartsID = tableID + '-highcharts';
                    $('<div id="' + highchartsID + '"  class="highcharts-wrapper ' + selector + '_wrapper"></div>').insertBefore(this);

                    // extend the specific preset with default preset and the data configuration
                    var extPreset = $.extend(true,
                        { data: { table: tableID } },
                        _config.highcharts_presets.default,
                        _config.highcharts_presets[selector]
                    );

                    // render the chart
                    Highcharts.chart(highchartsID, extPreset);

                    $(this).addClass(cls);

                    var highchartsContainer = $('#' + highchartsID);
                    if (highchartsContainer.is('.highcharts_2col_wrapper, .highcharts_col_stack_wrapper')) {
                        var data_array = highchartsContainer.highcharts().series[0].data;

                        highchartsContainer.find('.highcharts-xaxis-labels text').addClass('interactive').each(function (idx, obj) {
                            var data = data_array[idx];
                            $(obj).on('click', function () {
                                location.href = $(data.name).attr('href');
                            });
                        });
                    }
                });
            }
        );
    };

    var _activateBootstrapTabs = function () {
        $('#tab-nav').on('shown.bs.tab', 'a', function () {
            _renderChartsFromTables();
        }).find('a[href="' + document.location.hash.replace('!', '') + '"]').tab('show');

        $('select#select-tabs').on('change', function () {
            $(':selected', this).tab('show');
        }).on('shown.bs.tab', 'option', function () {
            _renderChartsFromTables();
        });
    };

    return {
        init: _init
    };
})();
